﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class AdministrationService
    {
        public static List<string> ExecutiveLogin(string userName, string password, ref string msg)
        {
            List<string> result = new List<string>();
            try
            {
                bool isLogin = AdministrationHelper.ExecutiveLogin(userName, password, ref msg);
                if (isLogin)
                {
                    result.Add("true");
                }
                else
                {
                    result.Add("false");
                    result.Add(msg);
                }
            }
            catch (Exception ex)
            {
                result.Add("false");
                result.Add(ex.Message);
            }
            return result;
        }
        public static List<MenuList> ExecMenuList(int roleid)
        {
            return AdministrationHelper.ExecMenuList(roleid);
        }
        public static bool IsExecutiveLogin(ref ExecuRegister lu)
        {
            return AdministrationHelper.IsExecutiveLogin(ref lu);
        }
        public static bool LogoutExecutive()
        {
            return AdministrationHelper.LogoutExecutive();
        }
    }
}