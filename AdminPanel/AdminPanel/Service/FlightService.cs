﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;

namespace AdminPanel.Service
{
    public static class FlightService
    {
        public static FlightModel GetTicketReport(string userType, string loginID, FlightModel model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.ToString("yyyy-MM-dd") + " 12:00 AM";
            }
            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return FlightHelper.GetTicketReport(userType, loginID, model);
        }
    }
}