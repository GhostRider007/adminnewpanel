﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class FlightSettingService
    {
        #region[FareTypeMaster]
        internal static List<FareTypeMaster> FareTypeMasterList()
        {
            return FlightSettingHelper.FareTypeMasterList();
        }
        internal static bool UpdateFareTypeMaster(FareTypeMaster model)
        {
            return FlightSettingHelper.UpdateFareTypeMaster(model);
        }
        #endregion
    }
}