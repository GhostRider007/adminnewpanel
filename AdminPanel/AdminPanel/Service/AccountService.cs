﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class AccountService
    {
        public static LedgerModel GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
            }

            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return AccountHelper.GetLedgerDetails(userType, loginID, model);
        }
        public static LedgerModel GetSaleRejister(string userType, string loginID, LedgerModel model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
            }

            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return AccountHelper.GetSaleRejister(userType, loginID, model);
        }
        public static LedgerModel GetUnflowReport(string userType, string loginID, LedgerModel model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
            }

            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return AccountHelper.GetUnflowReport(userType, loginID, model);
        }
        public static LedgerModel GetCreditLimit(string userType, string loginID, LedgerModel model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            else
            {
                DateTime dtNow = DateTime.Now;
                model.FilterFromDate = dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
            }

            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return AccountHelper.GetCreditLimit(userType, loginID, model);
        }
        public static Dictionary<string, string> GetBookingTypeFromLedger()
        {
            return AccountHelper.GetBookingTypeFromLedger();
        }
        public static Dictionary<string, string> GetTransTypeFromLedger()
        {
            return AccountHelper.GetTransTypeFromLedger();
        }
        public static PaymentDepositModel GetDepositStatusDetail(string status, string userid="", string accountId = "")
        {
            return AccountHelper.GetDepositStatusDetail(status, userid, accountId);
        }
        public static PaymentDepositModel GetDepositDetailsByID(string deptid)
        {
            return AccountHelper.GetDepositDetailsByID(deptid);
        }
        public static int UpdateDepositDetails(string deptid, string agentId, string status, string type, string accountId, string remark)
        {
            return AccountHelper.UpdateDepositDetails(deptid, agentId, status, type, accountId, remark);
        }
    }
}