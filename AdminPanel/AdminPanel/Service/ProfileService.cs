﻿using AdminPanel.Helper;
using AdminPanel.Models;
using System;
using System.Collections.Generic;

namespace AdminPanel.Service
{
    public static class ProfileService
    {
        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
            return ProfileHelper.ChangePassword(userid, oldpassword, newpassword);
        }
        #endregion

        #region[AgencyTDS]
        public static string AgencyList_ddl()
        {
            return ProfileHelper.AgencyList_ddl();
        }
        public static AgencyDetail GetAgency(string userid)
        {
            return ProfileHelper.GetAgency(userid);
        }
        public static bool UpdateAgencyTDS(int id, string tds)
        {
            return ProfileHelper.UpdateAgencyTDS(id, tds);
        }
        #endregion
        public static Dictionary<string, string> GetAgencyTypeList()
        {
            return ProfileHelper.GetAgencyTypeList();
        }
        public static Dictionary<string, string> GetSalesRef()
        {
            return ProfileHelper.GetSalesRef();
        }
        public static Agency GetAgencyList(Agency model)
        {
            if (!string.IsNullOrEmpty(model.FilterFromDate))
            {
                model.FilterFromDate = UtilityClass.FilterDateFormate(model.FilterFromDate, "from");
            }
            if (!string.IsNullOrEmpty(model.FilterToDate))
            {
                model.FilterToDate = UtilityClass.FilterDateFormate(model.FilterToDate, "to");
            }
            return ProfileHelper.GetAgencyList(model);
        }
        public static SingleAgency_SP GetAgencyDetailsByUserId(string userId)
        {
            return ProfileHelper.GetAgencyDetailsByUserId(userId);
        }
        public static Dictionary<string, string> GetDistrdetails()
        {
            return ProfileHelper.GetDistrdetails();
        }
        public static Dictionary<string, string> GetAgentGroupType()
        {
            return ProfileHelper.GetAgentGroupType();
        }
        public static Dictionary<string, string> GetStateList()
        {
            return ProfileHelper.GetStateList();
        }
        public static Dictionary<string, string> GetCityList(string stateCode)
        {
            return ProfileHelper.GetCityList(stateCode);
        }
        public static string UpdateAgencyDetail(SingleAgency_SP model)
        {
            if (ProfileHelper.UpdateAgencyDetail(model) > 0)
            {
                return "success";
            }
            return "failed"; ;
        }
        public static bool InsertAgencyRegistration(AgencyDetail model)
        {
            if (ProfileHelper.InsertAgencyRegistration(model) > 0)
            {
                return true;
            }
            return false;
        }
    }
}