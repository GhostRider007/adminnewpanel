﻿using System;
using System.Configuration;

namespace AdminPanel.Models
{
    public static class ConfigFile
    {
        public static string DatabaseConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["dtconnection"].ConnectionString; }
        }
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }
        public static string MerchantKey
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["MerchantKey"]); }
        }
        public static string WebsiteLogo
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitelogo"]); }
        }
        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }
    }
}