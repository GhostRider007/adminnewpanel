﻿namespace AdminPanel.Models
{
    public class BankDetails
    {
        public int id { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Area { get; set; }
        public string AccountNumber { get; set; }
        public string NEFTCode { get; set; }
        public string CreatedDate { get; set; }
        public string DISTRID { get; set; }
    }
}