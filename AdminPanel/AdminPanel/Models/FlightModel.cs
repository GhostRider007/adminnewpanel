﻿using System.Collections.Generic;

namespace AdminPanel.Models
{
    public class FlightModel
    {
        public string TotalAfterDis { get; set; }
        public string TotalFare { get; set; }
        public string OrderId { get; set; }
        public string PgEmail { get; set; }
        public string PgMobile { get; set; }
        public string TotalBookingCost { get; set; }
        public string Sector { get; set; }
        public string AgentId { get; set; }
        public string AgencyName { get; set; }
        public string AgentType { get; set; }
        public string PaxId { get; set; }
        public string PaxType { get; set; }
        public string Status { get; set; }
        public string CreateDate { get; set; }
        public string VC { get; set; }
        public string AirlineLogo { get; set; }
        public string GdsPnr { get; set; }
        public string TicketNumber { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string ExecutiveId { get; set; }
        public string Trip { get; set; }
        public string RejectedRemark { get; set; }
        public string Provider { get; set; }
        public string FareType { get; set; }
        public string PartnerName { get; set; }
        public string FTYPE { get; set; }
        public string JourneyDate { get; set; }
        public string PaymentMode { get; set; }
        public string CountFound { get; set; }
        public string PgCharges { get; set; }
        public string PName { get; set; }
        public string FareRule { get; set; }
        public int TotalCount { get; set; }
        public decimal TotalTicketAmount { get; set; }
        public int TotalTicketIssued { get; set; }
        public List<FlightModel> TicketReportList { get; set; }
        public string FilterFromDate { get; set; }
        public string FilterToDate { get; set; }
        public string FilterAgencyId { get; set; }
        public string FilterPnr { get; set; }
        public string FilterAirline { get; set; }
        public string FilterAirlineCode { get; set; }
        public string FilterPaxName { get; set; }
        public string FilterOrderId { get; set; }
        public string FilterTicketNo { get; set; }
        public string FilterTripType { get; set; }
        public string FilterAgencyName { get; set; }
    }
    public enum StatusClass
    {
        Pending,
        InProcess,
        Ticketed,
        Rejected,
        Cancelled,
        CancelRequest,
        CancelInprocess,
        CancelRejecRejectt,
        ReIssueRequest,
        ReIssueInProcess,
        ReIssue,
        ProxyRequest,
        ProxyInProcess,
        Confirm,
        Hold,
        Request
    }
}