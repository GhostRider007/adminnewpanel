﻿using System;
using System.Net;
using System.Net.Sockets;

namespace AdminPanel.Models
{
    public static class UtilityClass
    {
        public static string GetLocalIPAddress()
        {
            string result = string.Empty;

            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    result = ip.ToString();
                }
            }

            return result;
        }
        public static string FilterDateFormate(string date, string type)
        {
            if (!string.IsNullOrEmpty(date))
            {
                string[] dateSplit = date.Split('/');
                if (type == "from")
                {
                    return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + " 12:00:00 AM";//10/09/2021 12:00:00 AM
                }
                else
                {
                    return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0] + " 11:59:59 PM";//10/09/2021 11:59:59 PM
                }
            }

            return string.Empty;
        }
        public static string DisplayDateFormate(string date)
        {
            if (!string.IsNullOrEmpty(date))
            {
                DateTime dateTime = new DateTime();
                dateTime = Convert.ToDateTime(date);
                return dateTime.ToString("dd MMM yyyy hh:mm tt");
            }

            return string.Empty;
        }
        public static string FilterTodayDate()
        {
            DateTime dtNow = DateTime.Now;
            return dtNow.Year.ToString() + "-" + dtNow.Month.ToString() + "-" + dtNow.Day.ToString() + " 12:00:00 AM";
        }
    }
}