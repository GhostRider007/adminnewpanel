﻿namespace AdminPanel.Models
{
    public class Branch
    {
        public int id { get; set; }
        public string BranchName { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string Status { get; set; }
    }
    public class CityMaster
    {
        public int id { get; set; }
        public string CITY { get; set; }
        public string STATEID { get; set; }
        public string STATE { get; set; }
        public string CREATEDDATE { get; set; }
    }
    public class Executive
    {
        public int id { get; set; }
        public string user_id { get; set; }
        public string password { get; set; }
        public string role_id { get; set; }
        public string role { get; set; }
        public string role_type { get; set; }
        public string name { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string trip { get; set; }
        public string Branch { get; set; }
        public string BranchId { get; set; }
        public string IsSupplier { get; set; }
        public string Status { get; set; }
    }
}