﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AdminPanel.Models
{
    public class LedgerModel
    {
        public string AgencyID { get; set; }
        public string OrderId { get; set; }
        public string UserId { get; set; }
        public string YQ { get; set; }
        public string Tax { get; set; }
        public string servicetax { get; set; }
        public string MgtFee { get; set; }

        public string Commsion { get; set; }
        public string Tds { get; set; }
        public string GrossFare { get; set; }
        public string InvoiceTotal { get; set; }
        public string BookingDate { get; set; }
        public string TransactionFee { get; set; }
        public string PgCharges { get; set; }
        public string GstNo { get; set; }
        public string GST_Company_Name { get; set; }
        public string PaymentMode { get; set; }
        public string basefare { get; set; }
        public string FlightNumber { get; set; }
        public string Paxtype { get; set; }
        public string AirlinePnr { get; set; }
        public string AgencyName { get; set; }
        public string Sector { get; set; }
        public string InvoiceNo { get; set; }
        public string PaxName { get; set; }
        public string PnrNo { get; set; }
        public string Aircode { get; set; }
        public string TicketNo { get; set; }
        public string PassengerName { get; set; }
        public string TicketingCarrier { get; set; }
        public string AccountID { get; set; }
        public string ExecutiveID { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Aval_Balance { get; set; }
        public string CreatedDate { get; set; }
        public string BookingType { get; set; }
        public string Remark { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string DueAmount { get; set; }
        public string CreditLimit { get; set; }
        public string AgentID { get; set; }
        public string TransType { get; set; }
        public List<LedgerModel> LedgerList { get; set; }
        public int TotalCount { get; set; }
        public decimal TotalDebit { get; set; }
        public decimal TotalCredit { get; set; }


        public string FilterBookingType { get; set; }
        public List<SelectListItem> BookingTypeList { get; set; }
        public string FilterTransType { get; set; }
        public List<SelectListItem> TransTypeList { get; set; }
        public string FilterFromDate { get; set; }
        public string FilterSearchType { get; set; }
        public string FilterPaymentMode { get; set; }
        public string FilterToDate { get; set; }
        public string FilterAgencyId { get; set; }
        public string FilterAgencyName { get; set; }
        public string FilterProjectID { get; set; }
        public string FilterTicketNo { get; set; }
        public string FilterAirline { get; set; }
        public string FilterPNR { get; set; }
        public string FilterOderId { get; set; }
        public string FilterPaxName { get; set; }
        public string FilterTrip { get; set; }
        public string FilterDepartfrom { get; set; }
        public string FilterDepartTo { get; set; }
        public string FilterSalesPerson { get; set; }
    }

    public class PaymentDepositModel
    {
        public int Counter { get; set; }
        public string AgencyName { get; set; }
        public string AgencyID { get; set; }
        public decimal Amount { get; set; }
        public string ModeOfPayment { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string AccountNo { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string TransactionID { get; set; }
        public string BankAreaCode { get; set; }
        public string DepositCity { get; set; }
        public string DepositeDate { get; set; }
        public string Remark { get; set; }
        public string RemarkByAccounts { get; set; }
        public string Status { get; set; }
        public string AccountID { get; set; }
        public string SalesExecID { get; set; }
        public string UpdatedDateTime { get; set; }
        public string Date { get; set; }
        public string UploadType { get; set; }
        public string DepositeOffice { get; set; }
        public string ConcernPerson { get; set; }
        public string RecieptNo { get; set; }
        public string BranchCode { get; set; }
        public string AgentBankName { get; set; }
        public string AgentType { get; set; }
        public string AcceptedDateTime { get; set; }
        public string DISTRID { get; set; }
        public string InvoiceNo { get; set; }
        public bool IsMobile { get; set; }
        public string OTPRefNo { get; set; }
        public int TotalCount { get; set; }
        public List<PaymentDepositModel> PDList { get; set; }
    }
}