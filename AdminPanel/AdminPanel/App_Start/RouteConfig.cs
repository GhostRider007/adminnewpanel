﻿using System.Web.Mvc;
using System.Web.Routing;

namespace AdminPanel
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(name: "Login", url: "", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute(name: "Dashboard", url: "dashboard", defaults: new { controller = "Dashboard", action = "Dashboard", id = UrlParameter.Optional });

            //Account
            routes.MapRoute(name: "LedgerReport", url: "account/ledger-report", defaults: new { controller = "Account", action = "LedgerList", id = UrlParameter.Optional });
            routes.MapRoute(name: "UnflowReport", url: "account/unflow-report", defaults: new { controller = "Account", action = "UnflowReport", id = UrlParameter.Optional });
            routes.MapRoute(name: "CreditLimitHistory", url: "account/credit-limit-history", defaults: new { controller = "Account", action = "CreditLimitHistory", id = UrlParameter.Optional });
            routes.MapRoute(name: "SaleRegister", url: "account/sales-register", defaults: new { controller = "Account", action = "SaleRegister", id = UrlParameter.Optional });

            //Flight
            routes.MapRoute(name: "TicketReport", url: "flight/ticket-report", defaults: new { controller = "Flight", action = "TicketReport", id = UrlParameter.Optional });

            //Flight Setting
            routes.MapRoute(name: "FareTypeMaster", url: "flight-setting/fare-type-master", defaults: new { controller = "FlightSetting", action = "FareTypeMaster", id = UrlParameter.Optional });

            //Privilege Setting
            routes.MapRoute(name: "Branch", url: "privilege/branch-list", defaults: new { controller = "PrivilegeSetting", action = "Branch", id = UrlParameter.Optional });
            routes.MapRoute(name: "CityMaster", url: "privilege/city-list", defaults: new { controller = "PrivilegeSetting", action = "CityMaster", id = UrlParameter.Optional });
            routes.MapRoute(name: "Executive", url: "privilege/executive-list", defaults: new { controller = "PrivilegeSetting", action = "Executive", id = UrlParameter.Optional });
            routes.MapRoute(name: "MenuDetail", url: "privilege/page-list", defaults: new { controller = "PrivilegeSetting", action = "MenuDetail", id = UrlParameter.Optional });

            //Profile
            routes.MapRoute(name: "AgencyList", url: "profile/agency-list", defaults: new { controller = "Profile", action = "AgencyList", id = UrlParameter.Optional });
            routes.MapRoute(name: "ChangePassword", url: "profile/change-password", defaults: new { controller = "Profile", action = "ChangePassword", id = UrlParameter.Optional });

            //Register
            routes.MapRoute(name: "GroupTypeDetails", url: "register/group-type", defaults: new { controller = "Register", action = "GroupTypeDetails", id = UrlParameter.Optional });

            //Upload
            routes.MapRoute(name: "BankDetails", url: "upload/bank-list", defaults: new { controller = "Upload", action = "BankDetails", id = UrlParameter.Optional });
            routes.MapRoute(name: "AgencyCreditAndDebit", url: "upload/debit-credit-note", defaults: new { controller = "Upload", action = "AgencyCreditAndDebit", id = UrlParameter.Optional });
            routes.MapRoute(name: "SetAgencyCreditLimit", url: "upload/set-credit-limit", defaults: new { controller = "Upload", action = "SetAgencyCreditLimit", id = UrlParameter.Optional });

            routes.MapRoute(name: "Default", url: "{controller}/{action}/{id}", defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}
