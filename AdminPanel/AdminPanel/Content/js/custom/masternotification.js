﻿window.setInterval(function () { CallNotofication(); }, 1000000);
function CallNotofication() {
    $("#Notification").html("");
    $("#NotyCount").html("<i class='fa fa-spinner fa-pulse' style='font-size: 10px;'></i>");

    $.ajax({
        type: "Post",
        url: "/AutoComplete/FetchNotification",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                setTimeout(function () {
                    $("#Notification").html(data[0]);
                    $("#NotyCount").html(data[1]);
                    PlayAudio();
                }, 1000);
            }
            else {
                $("#NotyCount").html("0");
            }
        }
    });
}


function PlayAudio() {
    Audio.prototype.play = (function (play) {
        return function () {
            var audio = this,
                args = arguments,
                promise = play.apply(audio, args);
            if (promise !== undefined) {
                promise.catch(_ => {
                    // Autoplay was prevented. This is optional, but add a button to start playing.
                    var el = document.createElement("button");
                    el.innerHTML = "Play";
                    el.addEventListener("click", function () { play.apply(audio, args); });
                    this.parentNode.insertBefore(el, this.nextSibling)
                });
            }
        };
    })(Audio.prototype.play);

    // Try automatically playing our audio via script. This would normally trigger and error.
    document.getElementById('MyAudioElement').play();
}

function Logout() {
    $.ajax({
        type: "Post",
        url: "/Home/AgencyLogout",
        //data: '{username: ' + JSON.stringify(username) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data[0] == "success") {
                    window.location.href = data[1];
                }
            }
        }
    });
}