﻿using AdminPanel.Models;
using AdminPanel.Service;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class FlightController : Controller
    {
        public ActionResult TicketReport()
        {
            FlightModel model = new FlightModel();

            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                UpdateModel(model);
                model = FlightService.GetTicketReport(lu.role_type, lu.user_id, model);
            }
            return View(model);
        }
    }
}