﻿using AdminPanel.Models;
using AdminPanel.Service;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class ProfileController : Controller
    {
        public ActionResult AgencyList()
        {
            Agency model = new Agency();
            if (ModelState.IsValid)
            {
                ExecuRegister lu = new ExecuRegister();
                if (AdministrationService.IsExecutiveLogin(ref lu))
                {
                    UpdateModel(model);
                    model.FilterUserType = lu.user_type;
                    model.FilterDistrId = lu.user_id;

                    model = ProfileService.GetAgencyList(model);
                    model.AgencyTypeList = CommonClass.CommonPopulateList(ProfileService.GetAgencyTypeList());
                    model.SalesRefList = CommonClass.CommonPopulateList(ProfileService.GetSalesRef());
                }
                else
                {
                    return Redirect(ConfigFile.WebsiteUrl);
                }
            }
            return View(model);
        }
        public ActionResult ResetAgencyFilter()
        {
            ModelState.Clear();
            return RedirectToAction("agencylist");
        }

        #region[Change Password]
        public ActionResult ChangePassword(bool IsUpdated = false)
        {
            if (IsUpdated)
            {
                ViewBag.UpdateConfirmation = "Password has been updated";
            }
            else
            {
                ViewBag.UpdateConfirmation = "";
            }
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(string oldpassword, string newpassword)
        {
            Models.ExecuRegister lu = ((List<Models.ExecuRegister>)HttpContext.Session["executive"])[0];
            string userid = lu.user_id;

            bool isChanged = ProfileService.ChangePassword(userid, oldpassword, newpassword);

            if (isChanged)
            {
                return RedirectToAction("ChangePassword", "Profile", new { IsUpdated = true });
            }

            return RedirectToAction("ChangePassword", "Profile", new { IsUpdated = false });
        }
        #endregion

        #region[AgencyTDS]
        public ActionResult AgencyTDS(string userid = null)
        {
            if (Session["AgentList"] == null)
            {
                Session["AgentList"] = ProfileService.AgencyList_ddl();
            }
            ViewBag.AgentList = Session["AgentList"];

            AgencyDetail model = new AgencyDetail();

            if (!string.IsNullOrEmpty(userid))
            {
                model = ProfileService.GetAgency(userid);
            }
            return View(model);
        }
        public ActionResult EditTDS(string userid)
        {
            AgencyDetail model = ProfileService.GetAgency(userid);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditTDS(int id, string tds)
        {
            bool IsSuccess = ProfileService.UpdateAgencyTDS(id, tds);

            return RedirectToAction("AgencyTDS");

        }
        public ActionResult AgencyDetailById(string userid)
        {
            SingleAgency_SP model = new SingleAgency_SP();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                if (lu.user_type.Trim().ToUpper() == "SALES")
                {
                    model.IsSalesLogin = true;
                }
                model = ProfileService.GetAgencyDetailsByUserId(userid);
                model.DistrList = CommonClass.CommonPopulateList(ProfileService.GetDistrdetails());
                model.SalesRefList = CommonClass.CommonPopulateList(ProfileService.GetSalesRef());
                model.AgentGroupTypeList = CommonClass.CommonPopulateList(ProfileService.GetAgentGroupType());
                model.StateList = CommonClass.CommonPopulateList(ProfileService.GetStateList());
                model.CityList = CommonClass.CommonPopulateList(ProfileService.GetCityList(model.State));
                model.GstCityList = CommonClass.CommonPopulateList(ProfileService.GetCityList(model.GST_State));
            }
            return PartialView("_PartialSingleAgency", model);
        }
        public JsonResult GetStateList()
        {
            return Json(CommonClass.CommonPopulateList(ProfileService.GetStateList()), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCityList(string stateid)
        {
            return Json(CommonClass.CommonPopulateList(ProfileService.GetCityList(stateid)), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateAgencyDel(SingleAgency_SP model)
        {
            return Json(ProfileService.UpdateAgencyDetail(model));
        }
        public JsonResult InsertAgencyRegistration(AgencyDetail model)
        {
            return Json(ProfileService.InsertAgencyRegistration(model));
        }
        #endregion

        //[HttpPost]
        //public FileResult Export()
        //{
        //    Agency model = new Agency();
        //    ExecuRegister lu = new ExecuRegister();
        //    if (AdministrationService.IsExecutiveLogin(ref lu))
        //    {
        //        UpdateModel(model);
        //        model.FilterUserType = lu.user_type;
        //        model.FilterDistrId = lu.user_id;

        //        model = ProfileService.GetAgencyList(model);
        //    }

        //    var properties = typeof(Agency).GetProperties();


        //    foreach (var item in properties)
        //    {
        //        // Console.WriteLine(item.Name);
        //    }

        //    //NorthwindEntities entities = new NorthwindEntities();
        //    //DataTable dt = new DataTable("Grid");
        //    //dt.Columns.AddRange(new DataColumn[4] { new DataColumn("CustomerId"),
        //    //                                new DataColumn("ContactName"),
        //    //                                new DataColumn("City"),
        //    //                                new DataColumn("Country") });

        //    //var customers = from customer in entities.Customers.Take(10)
        //    //                select customer;

        //    //foreach (var customer in customers)
        //    //{
        //    //    dt.Rows.Add(customer.CustomerID, customer.ContactName, customer.City, customer.Country);
        //    //}

        //    //using (XLWorkbook wb = new XLWorkbook())
        //    //{
        //    //    wb.Worksheets.Add(dt);
        //    using (MemoryStream stream = new MemoryStream())
        //    {
        //       // wb.SaveAs(stream);
        //        return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Grid.xlsx");
        //    }
        //}
    }
}