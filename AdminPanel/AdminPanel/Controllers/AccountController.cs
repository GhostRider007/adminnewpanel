﻿using AdminPanel.Models;
using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult LedgerList()
        {
            LedgerModel model = new LedgerModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                UpdateModel(model);
                model = AccountService.GetLedgerDetails(lu.role_type, lu.user_id, model);
                model.BookingTypeList = CommonClass.CommonPopulateList(AccountService.GetBookingTypeFromLedger());
                model.TransTypeList = CommonClass.CommonPopulateList(AccountService.GetTransTypeFromLedger());
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }
        public ActionResult SaleRegister()
        {
            LedgerModel model = new LedgerModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                UpdateModel(model);
                model = AccountService.GetSaleRejister(lu.role_type, lu.user_id, model);
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }
        public ActionResult UnflowReport()
        {
            LedgerModel model = new LedgerModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                UpdateModel(model);
                model = AccountService.GetSaleRejister(lu.role_type, lu.user_id, model);
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }
        public ActionResult CreditLimitHistory()
        {
            LedgerModel model = new LedgerModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                UpdateModel(model);
                model = AccountService.GetCreditLimit(lu.role_type, lu.user_id, model);
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }
            return View(model);
        }
        public JsonResult GetBookingTypeFromLedger()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            Dictionary<string, string> pairList = AccountService.GetBookingTypeFromLedger();
            if (pairList != null && pairList.Count > 0)
            {
                foreach (var item in pairList)
                {
                    if (item.Key != "--Booking Type--")
                    {
                        items.Add(new SelectListItem
                        {
                            Text = item.Key.ToString(),
                            Value = item.Value.ToString()
                        });
                    }
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTransTypeFromLedger()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            Dictionary<string, string> pairList = AccountService.GetTransTypeFromLedger();
            if (pairList != null && pairList.Count > 0)
            {
                foreach (var item in pairList)
                {
                    if (item.Key != "--TransType--")
                    {
                        items.Add(new SelectListItem
                        {
                            Text = item.Key.ToString(),
                            Value = item.Value.ToString()
                        });
                    }
                }
            }
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAgencyLedgerDetail(string aid, string fd, string td, string bt, string tt)
        {
            string result = string.Empty;
            LedgerModel model = new LedgerModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                model.FilterAgencyId = aid;
                model.FilterFromDate = fd;
                model.FilterToDate = td;
                model.FilterBookingType = bt;
                model.FilterTransType = tt;
                model = AccountService.GetLedgerDetails(lu.role_type, lu.user_id, model);
                if (model.LedgerList != null && model.LedgerList.Count > 0)
                {
                    foreach (var item in model.LedgerList)
                    {
                        result += "<tr>";
                        result += "<td class='sticky-col first-col' style='background-color: #fff;'><a href='#' target='_blank' style='color: #fff; background-color: #17a2b8; border-color: #17a2b8; padding: 6px; border-radius: 2px;'>" + item.InvoiceNo + "</a></td>";
                        //result += "<td>" + item.AgencyID + "</td>";
                        //result += "<td>" + item.UserId + "</td>";
                        //result += "<td>" + item.AgencyName + "</td>";
                        result += "<td>" + item.Sector + "</td>";
                        result += "<td>" + item.PaxName + "</td>";
                        result += "<td>" + item.PnrNo + "</td>";
                        result += "<td>" + item.Aircode + "</td>";
                        result += "<td>" + item.TicketNo + "</td>";
                        result += "<td>" + item.Debit + "</td>";
                        result += "<td>" + item.Credit + "</td>";
                        result += "<td>" + item.Aval_Balance + "</td>";
                        result += "<td>" + item.BookingType + "</td>";
                        result += "<td>" + item.CreatedDate + "</td>";
                        result += "<td>" + item.Remark + "</td>";
                        result += "<td>" + item.DueAmount + "</td>";
                        result += "<td>" + item.CreditLimit + "</td>";
                        result += "</tr>";
                    }
                }
                else
                {
                    result = "<tr><td colspan='14'><p class='text-danger' style='font-size:15px;'>No record found!</td></tr>";
                }
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAgencyTicketBookingDetail(string aid, string fd, string td, string pnr, string oid, string pax, string tno, string aline, string trip)
        {
            string result = string.Empty;
            FlightModel model = new FlightModel();
            ExecuRegister lu = new ExecuRegister();
            if (AdministrationService.IsExecutiveLogin(ref lu))
            {
                model.FilterAgencyId = aid;
                model.FilterFromDate = fd;
                model.FilterToDate = td;
                model.FilterPnr = pnr;
                model.FilterOrderId = oid;
                model.FilterPaxName = pax;
                model.FilterTicketNo = tno;
                model.FilterAirline = aline;
                model.FilterTripType = trip;

                model = FlightService.GetTicketReport(lu.role_type, lu.user_id, model);
                if (model.TicketReportList != null && model.TicketReportList.Count > 0)
                {
                    foreach (var item in model.TicketReportList)
                    {
                        result += "<tr>";
                        result += "<td class='sticky-col first-col' style='background-color: #fff;'><a href='#' target='_blank' style='color: #fff; background-color: #17a2b8; border-color: #17a2b8; padding: 6px; border-radius: 2px;'>" + item.OrderId + "</a></td>";
                        result += "<td>" + item.CreateDate + "</td>";
                        result += "<td>" + item.GdsPnr + "</td>";
                        result += "<td>- - -</td>";
                        result += "<td>" + item.Sector + "</td>";
                        result += "<td>" + item.FName + " " + item.LName + "</td>";
                        result += "<td>" + item.VC + "</td>";
                        result += "<td>" + item.Trip + "</td>";
                        result += "<td>- - -</td>";
                        result += "<td>" + item.TotalBookingCost + "</td>";
                        result += "<td>" + item.TotalAfterDis + "</td>";
                        if (item.Status.ToLower().Trim() == "ticketed")
                        {
                            result += "<td><span style='font-weight: bold; background: #28a745; color: #fff; padding: 5px; border-radius: 2px;'><i class='fa fa-check-circle' aria-hidden='true'></i>" + item.Status + "</span></td>";
                        }
                        else
                        {
                            result += "<td>" + item.Status + "</td>";
                        }

                        result += "<td>- - -</td>";
                        result += "<td>" + item.ExecutiveId + "</td>";
                        result += "</tr>";
                    }
                }
                else
                {
                    result = "<tr><td colspan='14'><p class='text-danger' style='font-size:15px;'>No record found!</td></tr>";
                }
            }
            else
            {
                if (AdministrationService.LogoutExecutive())
                {
                    Response.Redirect("/");
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private string GetDepositStatusDetail(string userid, string status)
        {
            string result = string.Empty;
            PaymentDepositModel model = new PaymentDepositModel();
            try
            {
                model = AccountService.GetDepositStatusDetail(status, userid);
                if (model.PDList != null && model.PDList.Count > 0)
                {
                    foreach (var item in model.PDList)
                    {
                        result += "<tr>";
                        if (item.Status == "Confirm")
                        {
                            result += "<td><span class='text-success' style='font-weight: bold;text-transform: uppercase;'>" + item.Status + "</span></td>";
                        }
                        else if (item.Status == "InProcess")
                        {
                            result += "<td class='sticky-col first-col' style='background-color: #fff;'><a id='OpenCredit_" + item.Counter + "' href='javascript:;' style='color: #fff; background-color: #ffc107; border-color: #17a2b8; padding: 6px; border-radius: 2px;' class='creditdeposit' data-depositid='" + item.Counter + "'>Credit</a></td>";
                        }
                        else
                        {
                            result += "<td class='sticky-col first-col' style='background-color: #fff;'><a href='javascript:;' style='color: #fff; background-color: #28a745; border-color: #17a2b8; padding: 6px; border-radius: 2px;' class='acceptdeposit' data-depositid='" + item.Counter + "'>Accept</a>&nbsp;<a href='javascript:;' style='color: #fff; background-color: #dc3545; border-color: #17a2b8; padding: 6px; border-radius: 2px;' class='rejectdeposit' data-depositid='" + item.Counter + "'>Reject</a><input type='hidden' id='hdnAgencyId' value='" + item.AgencyID + "'/></td>";
                        }

                        //result += "<td>" + item.Counter + "</td>";
                        //result += "<td><span class='text-success' onlick='AcceptDeposit(" + item.Counter + ")'>Accept</span><span class='text-danger' onlick='RejectDeposit(" + item.Counter + ")'>Reject</span></td>";
                        result += "<td>" + item.Date + "</td>";
                        if (item.Status == "Confirm")
                        {
                            result += "<td><span class='text-success'>" + item.Status + "</span></td>";
                        }
                        else if (item.Status == "InProcess")
                        {
                            result += "<td><span class='text-warning'>" + item.Status + "</span></td>";
                        }
                        else
                        {
                            result += "<td><span class='text-danger'>" + item.Status + "</span></td>";
                        }
                        result += "<td>" + item.Amount + "</td>";
                        result += "<td>" + item.ModeOfPayment + "</td>";
                        result += "<td>" + item.BankName + "</td>";
                        result += "<td>" + item.ChequeNo + "</td>";
                        result += "<td>" + item.ChequeDate + "</td>";
                        result += "<td>" + item.TransactionID + "</td>";
                        result += "<td>" + item.BankAreaCode + "</td>";
                        //if (item.Status.ToLower().Trim() == "ticketed")
                        //{
                        //    result += "<td><span style='font-weight: bold; background: #28a745; color: #fff; padding: 5px; border-radius: 2px;'><i class='fa fa-check-circle' aria-hidden='true'></i>" + item.Status + "</span></td>";
                        //}
                        //else
                        //{
                        //    result += "<td>" + item.Status + "</td>";
                        //}

                        result += "<td>" + item.DepositCity + "</td>";
                        result += "<td>" + item.Remark + "</td>";
                        result += "</tr>";
                        if (item.Status == "InProcess")
                        {
                            result += "<tr id='trCredit_"+ item.Counter + "' data-creditid='"+ item.Counter + "' class='trinnercredit' style='display:none;'><td class='sticky-col first-col' style='background-color: #fff;'>&nbsp;</td>";
                            result += "<td colspan='4'><textarea id='txtCreditRemark_" + item.Counter + "' class='form-control commoncss'></textarea></td>";
                            result += "<td colspan='2'><input id='txtCreditAmount_" + item.Counter + "' type='text' value='2000' class='form-control commoncss' readonly/></td>";
                            result += "<td colspan='2'>";
                            result += "<select id='ddlCreditType_" + item.Counter + "' class='form-control commoncss'><option value='CA'>Cash</option><option value='CC'>Card</option></select>";
                            result += "</td>";
                            result += "<td colspan='3'><button type='submit' class='btn btn-success btn-xs givecredit' data-givecreditid='"+ item.Counter + "' id='btnGiveCredit_" + item.Counter + "'>Credit</button></td>";
                            result += "</tr>";
                        }
                    }
                }
                else
                {
                    result = "<tr><td colspan='12'><p class='text-danger' style='font-size:15px;'>No record found!</td></tr>";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public JsonResult GetPendingDepositStatus(string userid, string status)
        {
            return Json(GetDepositStatusDetail(userid, status), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateDepositDetails(string deptid, string agentid, string remark, string updatestatus, string status)
        {
            List<string> result = new List<string>();
            try
            {
                if (!string.IsNullOrEmpty(deptid))
                {
                    PaymentDepositModel model = AccountService.GetDepositDetailsByID(deptid);
                    if (model != null && model.Counter > 0)
                    {
                        ExecuRegister lu = new ExecuRegister();
                        if (AdministrationService.IsExecutiveLogin(ref lu))
                        {
                            if (updatestatus == "accept")
                            {
                                if (AccountService.UpdateDepositDetails(deptid, agentid, "InProcess", "Acc", lu.user_id, remark) > 0)
                                {
                                    result.Add("success");
                                }
                            }
                            else if (updatestatus == "reject")
                            {
                            }

                            result.Add(GetDepositStatusDetail(agentid, status));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}