﻿using AdminPanel.Service;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class AutoCompleteController : Controller
    {
        public JsonResult FetchAgencyList(string searchinput)
        {
            return Json(AutoCompleteService.GetAgencyAutoSearch(searchinput));
        }
        public JsonResult FetchAirlineList(string searchinput)
        {
            return Json(AutoCompleteService.GetAirlineSearch(searchinput));
        }

        public JsonResult FetchNotification()
        {
            return Json(AutoCompleteService.GetNotificationDetails());
        }
    }
}