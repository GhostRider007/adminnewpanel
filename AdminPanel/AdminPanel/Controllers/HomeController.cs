﻿using AdminPanel.Models;
using AdminPanel.Service;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AdminPanel.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Menu(ExecuRegister lu)
        {
            MenuList model = new MenuList();
            if (!string.IsNullOrEmpty(lu.role_id.ToString()))
            {
                model.ExecMenuList = AdministrationService.ExecMenuList(Convert.ToInt32(lu.role_id));
            }
            return View(model);
        }
        public JsonResult ProcessBackendLogin(string username, string password)
        {
            string msg = string.Empty;
            return Json(AdministrationService.ExecutiveLogin(username, password, ref msg));
        }
        public JsonResult BackendLogOut()
        {
            return Json(AdministrationService.LogoutExecutive());
        }
        public JsonResult AgencyLogout()
        {
            List<string> result = new List<string>();
            if (AdministrationService.LogoutExecutive())
            {
                result.Add("success");
                result.Add(ConfigFile.WebsiteUrl);
            }
            return Json(result);
        }
    }
}