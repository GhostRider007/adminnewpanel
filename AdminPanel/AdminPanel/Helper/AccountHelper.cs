﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;

namespace AdminPanel.Helper
{
    public static class AccountHelper
    {
        public static LedgerModel GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {
            LedgerModel result = new LedgerModel();

            try
            {
                DataTable dtLedger = AccountDatabase.GetLedgerDetails(userType, loginID, model);
                if (dtLedger != null && dtLedger.Rows.Count > 0)
                {
                    decimal tempCredit = 0;
                    decimal tempDebit = 0;
                    List<LedgerModel> ledList = new List<LedgerModel>();
                    for (int i = 0; i < dtLedger.Rows.Count; i++)
                    {
                        LedgerModel ledger = new LedgerModel();
                        ledger.AgencyID = dtLedger.Rows[i]["AgencyID"].ToString();
                        ledger.UserId = dtLedger.Rows[i]["UserId"].ToString();
                        ledger.AgencyName = dtLedger.Rows[i]["AgencyName"].ToString();
                        ledger.Sector = dtLedger.Rows[i]["Sector"].ToString();
                        ledger.InvoiceNo = dtLedger.Rows[i]["InvoiceNo"].ToString();
                        ledger.PaxName = dtLedger.Rows[i]["PaxName"].ToString();
                        ledger.PnrNo = !string.IsNullOrEmpty(dtLedger.Rows[i]["PnrNo"].ToString()) ? dtLedger.Rows[i]["PnrNo"].ToString() : "- - -";
                        ledger.Aircode = !string.IsNullOrEmpty(dtLedger.Rows[i]["Aircode"].ToString()) ? dtLedger.Rows[i]["Aircode"].ToString() : "- - -";
                        ledger.TicketNo = !string.IsNullOrEmpty(dtLedger.Rows[i]["TicketNo"].ToString()) ? dtLedger.Rows[i]["TicketNo"].ToString() : "- - -";
                        //ledger.PassengerName = dtLedger.Rows[i]["PassengerName"].ToString();
                        ledger.TicketingCarrier = dtLedger.Rows[i]["TicketingCarrier"].ToString();
                        ledger.AccountID = dtLedger.Rows[i]["AccountID"].ToString();
                        ledger.ExecutiveID = dtLedger.Rows[i]["ExecutiveID"].ToString();
                        ledger.Debit = dtLedger.Rows[i]["Debit"].ToString();
                        tempDebit = tempDebit + Convert.ToDecimal(ledger.Debit);
                        ledger.Credit = dtLedger.Rows[i]["Credit"].ToString();
                        tempCredit = tempCredit + Convert.ToDecimal(ledger.Credit);
                        ledger.Aval_Balance = dtLedger.Rows[i]["Aval_Balance"].ToString();
                        ledger.CreatedDate = UtilityClass.DisplayDateFormate(dtLedger.Rows[i]["CreatedDate"].ToString());
                        ledger.BookingType = dtLedger.Rows[i]["BookingType"].ToString();
                        ledger.Remark = !string.IsNullOrEmpty(dtLedger.Rows[i]["Remark"].ToString()) ? dtLedger.Rows[i]["Remark"].ToString() : "- - -";
                        ledger.C = dtLedger.Rows[i]["C"].ToString();
                        ledger.D = dtLedger.Rows[i]["D"].ToString();
                        ledger.DueAmount = dtLedger.Rows[i]["DueAmount"].ToString();
                        ledger.CreditLimit = dtLedger.Rows[i]["CreditLimit"].ToString();
                        ledger.AgentID = dtLedger.Rows[i]["AgentID"].ToString();
                        ledger.TransType = dtLedger.Rows[i]["TransType"].ToString();
                        ledList.Add(ledger);
                    }
                    result.LedgerList = ledList;
                    result.TotalCount = ledList.Count;
                    result.TotalDebit = tempDebit;
                    result.TotalCredit = tempCredit;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static Dictionary<string, string> GetBookingTypeFromLedger()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtBookingType = AccountDatabase.GetBookingTypeFromLedger();
                if (dtBookingType != null && dtBookingType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtBookingType.Rows.Count; i++)
                    {
                        pairList.Add(dtBookingType.Rows[i]["BookingType"].ToString(), dtBookingType.Rows[i]["BookingType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetTransTypeFromLedger()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtTransType = AccountDatabase.GetTransTypeFromLedger();
                if (dtTransType != null && dtTransType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtTransType.Rows.Count; i++)
                    {
                        pairList.Add(dtTransType.Rows[i]["TransType"].ToString(), dtTransType.Rows[i]["TransType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static LedgerModel GetSaleRejister(string userType, string loginID, LedgerModel model)
        {
            LedgerModel result = new LedgerModel();

            try
            {
                DataTable dtSaleReg = AccountDatabase.GetSaleRejister(userType, loginID, model);
                if (dtSaleReg != null && dtSaleReg.Rows.Count > 0)
                {
                    List<LedgerModel> SaleList = new List<LedgerModel>();
                    for (int i = 0; i < dtSaleReg.Rows.Count; i++)
                    {
                        LedgerModel Saleledger = new LedgerModel();
                        Saleledger.OrderId = dtSaleReg.Rows[i]["OrderId"].ToString();
                        Saleledger.AgencyID = dtSaleReg.Rows[i]["AgencyID"].ToString();
                        Saleledger.AgencyName = dtSaleReg.Rows[i]["AgentCompany"].ToString();
                        Saleledger.PnrNo = dtSaleReg.Rows[i]["GdsPnr"].ToString();
                        Saleledger.AirlinePnr = dtSaleReg.Rows[i]["AirlinePnr"].ToString();
                        Saleledger.Sector = dtSaleReg.Rows[i]["Sector"].ToString();
                        Saleledger.Aircode = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["AirlineCode"].ToString()) ? dtSaleReg.Rows[i]["AirlineCode"].ToString() : "- - -";
                        Saleledger.PassengerName = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["PassengerName"].ToString()) ? dtSaleReg.Rows[i]["PassengerName"].ToString() : "- - -";
                        Saleledger.Paxtype = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["Paxtype"].ToString()) ? dtSaleReg.Rows[i]["Paxtype"].ToString() : "- - -";
                        Saleledger.FlightNumber = dtSaleReg.Rows[i]["FlightNumber"].ToString();
                        Saleledger.TicketNo = dtSaleReg.Rows[i]["Ticketnumber"].ToString();
                        Saleledger.basefare = dtSaleReg.Rows[i]["basefare"].ToString();
                        Saleledger.YQ = dtSaleReg.Rows[i]["YQ"].ToString();
                        Saleledger.Tax = dtSaleReg.Rows[i]["Tax"].ToString();
                        Saleledger.servicetax = dtSaleReg.Rows[i]["servicetax"].ToString();
                        Saleledger.TransactionFee = dtSaleReg.Rows[i]["TransactionFee"].ToString();
                        Saleledger.MgtFee = dtSaleReg.Rows[i]["MgtFee"].ToString();
                        Saleledger.Commsion = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["Commsion"].ToString()) ? dtSaleReg.Rows[i]["Commsion"].ToString() : "- - -";
                        Saleledger.Tds = dtSaleReg.Rows[i]["Tds"].ToString();
                        Saleledger.GrossFare = dtSaleReg.Rows[i]["GrossFare"].ToString();
                        Saleledger.InvoiceTotal = dtSaleReg.Rows[i]["InvoiceTotal"].ToString();
                        Saleledger.BookingDate = dtSaleReg.Rows[i]["BookingDate"].ToString();
                        Saleledger.PaymentMode = dtSaleReg.Rows[i]["PaymentMode"].ToString();
                        Saleledger.PgCharges = dtSaleReg.Rows[i]["PgCharges"].ToString();
                        Saleledger.GST_Company_Name = dtSaleReg.Rows[i]["GST_Company_Name"].ToString();
                        Saleledger.GstNo = dtSaleReg.Rows[i]["GstNo"].ToString();
                        SaleList.Add(Saleledger);
                    }
                    result.LedgerList = SaleList;
                    result.TotalCount = SaleList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static LedgerModel GetUnflowReport(string userType, string loginID, LedgerModel model)
        {
            LedgerModel result = new LedgerModel();

            try
            {
                DataTable dtSaleReg = AccountDatabase.GetUnflowReport(userType, loginID, model);
                if (dtSaleReg != null && dtSaleReg.Rows.Count > 0)
                {
                    List<LedgerModel> SaleList = new List<LedgerModel>();
                    for (int i = 0; i < dtSaleReg.Rows.Count; i++)
                    {
                        LedgerModel Saleledger = new LedgerModel();
                        Saleledger.OrderId = dtSaleReg.Rows[i]["OrderId"].ToString();
                        Saleledger.AgencyID = dtSaleReg.Rows[i]["AgencyID"].ToString();
                        Saleledger.AgencyName = dtSaleReg.Rows[i]["AgentCompany"].ToString();
                        Saleledger.PnrNo = dtSaleReg.Rows[i]["GdsPnr"].ToString();
                        Saleledger.AirlinePnr = dtSaleReg.Rows[i]["AirlinePnr"].ToString();
                        Saleledger.Sector = dtSaleReg.Rows[i]["Sector"].ToString();
                        Saleledger.Aircode = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["AirlineCode"].ToString()) ? dtSaleReg.Rows[i]["AirlineCode"].ToString() : "- - -";
                        Saleledger.PassengerName = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["PassengerName"].ToString()) ? dtSaleReg.Rows[i]["PassengerName"].ToString() : "- - -";
                        Saleledger.Paxtype = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["Paxtype"].ToString()) ? dtSaleReg.Rows[i]["Paxtype"].ToString() : "- - -";
                        Saleledger.FlightNumber = dtSaleReg.Rows[i]["FlightNumber"].ToString();
                        Saleledger.TicketNo = dtSaleReg.Rows[i]["Ticketnumber"].ToString();
                        Saleledger.basefare = dtSaleReg.Rows[i]["basefare"].ToString();
                        Saleledger.YQ = dtSaleReg.Rows[i]["YQ"].ToString();
                        Saleledger.Tax = dtSaleReg.Rows[i]["Tax"].ToString();
                        Saleledger.servicetax = dtSaleReg.Rows[i]["servicetax"].ToString();
                        Saleledger.TransactionFee = dtSaleReg.Rows[i]["TransactionFee"].ToString();
                        Saleledger.MgtFee = dtSaleReg.Rows[i]["MgtFee"].ToString();
                        Saleledger.Commsion = !string.IsNullOrEmpty(dtSaleReg.Rows[i]["Commsion"].ToString()) ? dtSaleReg.Rows[i]["Commsion"].ToString() : "- - -";
                        Saleledger.Tds = dtSaleReg.Rows[i]["Tds"].ToString();
                        Saleledger.GrossFare = dtSaleReg.Rows[i]["GrossFare"].ToString();
                        Saleledger.InvoiceTotal = dtSaleReg.Rows[i]["InvoiceTotal"].ToString();
                        Saleledger.BookingDate = dtSaleReg.Rows[i]["BookingDate"].ToString();
                        Saleledger.PaymentMode = dtSaleReg.Rows[i]["PaymentMode"].ToString();
                        Saleledger.PgCharges = dtSaleReg.Rows[i]["PgCharges"].ToString();
                        Saleledger.GST_Company_Name = dtSaleReg.Rows[i]["GST_Company_Name"].ToString();
                        Saleledger.GstNo = dtSaleReg.Rows[i]["GstNo"].ToString();
                        SaleList.Add(Saleledger);
                    }
                    result.LedgerList = SaleList;
                    result.TotalCount = SaleList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static LedgerModel GetCreditLimit(string userType, string loginID, LedgerModel model)
        {
            LedgerModel result = new LedgerModel();

            try
            {
                DataTable dtSaleReg = AccountDatabase.GetCreditLimit(userType, loginID, model);
                if (dtSaleReg != null && dtSaleReg.Rows.Count > 0)
                {
                    List<LedgerModel> SaleList = new List<LedgerModel>();
                    for (int i = 0; i < dtSaleReg.Rows.Count; i++)
                    {
                        LedgerModel Saleledger = new LedgerModel();
                        Saleledger.UserId = dtSaleReg.Rows[i]["AgentID"].ToString();
                        Saleledger.AgencyID = dtSaleReg.Rows[i]["AgencyId"].ToString();
                        Saleledger.AgencyName = dtSaleReg.Rows[i]["AgencyName"].ToString();
                        Saleledger.CreditLimit = dtSaleReg.Rows[i]["CurrentCrdLimit"].ToString();
                        Saleledger.Remark = dtSaleReg.Rows[i]["Remark"].ToString();
                        Saleledger.ExecutiveID = dtSaleReg.Rows[i]["ExecutiveID"].ToString();
                        Saleledger.CreatedDate = dtSaleReg.Rows[i]["CreatedDate"].ToString();
                        SaleList.Add(Saleledger);
                    }
                    result.LedgerList = SaleList;
                    result.TotalCount = SaleList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static PaymentDepositModel GetDepositStatusDetail(string status, string userid = "", string accountId = "")
        {
            PaymentDepositModel result = new PaymentDepositModel();

            try
            {
                DataTable dtDeposit = AccountDatabase.GetDepositStatusDetail(status, userid, accountId);
                if (dtDeposit != null && dtDeposit.Rows.Count > 0)
                {
                    List<PaymentDepositModel> depositList = new List<PaymentDepositModel>();
                    for (int i = 0; i < dtDeposit.Rows.Count; i++)
                    {
                        PaymentDepositModel deposit = new PaymentDepositModel();

                        deposit.Counter = Convert.ToInt32(dtDeposit.Rows[i]["Counter"].ToString());
                        deposit.AgencyName = dtDeposit.Rows[i]["AgencyName"].ToString();
                        deposit.AgencyID = dtDeposit.Rows[i]["AgencyID"].ToString();
                        deposit.Amount = !string.IsNullOrEmpty(dtDeposit.Rows[i]["Amount"].ToString()) ? Convert.ToDecimal(dtDeposit.Rows[i]["Amount"].ToString()) : 0;
                        deposit.ModeOfPayment = dtDeposit.Rows[i]["ModeOfPayment"].ToString();
                        deposit.BankName = !string.IsNullOrEmpty(dtDeposit.Rows[i]["BankName"].ToString()) ? dtDeposit.Rows[i]["BankName"].ToString() : "- - -";
                        deposit.BranchName = dtDeposit.Rows[i]["BranchName"].ToString();
                        deposit.AccountNo = dtDeposit.Rows[i]["AccountNo"].ToString();
                        deposit.ChequeNo = !string.IsNullOrEmpty(dtDeposit.Rows[i]["ChequeNo"].ToString()) ? dtDeposit.Rows[i]["ChequeNo"].ToString() : "- - -";
                        deposit.ChequeDate = !string.IsNullOrEmpty(dtDeposit.Rows[i]["ChequeDate"].ToString()) ? dtDeposit.Rows[i]["ChequeDate"].ToString() : "- - -";
                        deposit.TransactionID = !string.IsNullOrEmpty(dtDeposit.Rows[i]["TransactionID"].ToString()) ? dtDeposit.Rows[i]["TransactionID"].ToString() : "- - -";
                        deposit.BankAreaCode = dtDeposit.Rows[i]["BankAreaCode"].ToString();
                        deposit.DepositCity = !string.IsNullOrEmpty(dtDeposit.Rows[i]["DepositCity"].ToString()) ? dtDeposit.Rows[i]["DepositCity"].ToString() : "- - -";
                        deposit.DepositeDate = dtDeposit.Rows[i]["DepositeDate"].ToString();
                        deposit.Remark = !string.IsNullOrEmpty(dtDeposit.Rows[i]["Remark"].ToString()) ? dtDeposit.Rows[i]["Remark"].ToString() : "- - -";
                        deposit.RemarkByAccounts = dtDeposit.Rows[i]["RemarkByAccounts"].ToString();
                        deposit.Status = dtDeposit.Rows[i]["Status"].ToString();
                        deposit.AccountID = dtDeposit.Rows[i]["AccountID"].ToString();
                        deposit.SalesExecID = dtDeposit.Rows[i]["SalesExecID"].ToString();
                        deposit.UpdatedDateTime = dtDeposit.Rows[i]["UpdatedDateTime"].ToString();
                        deposit.Date = UtilityClass.DisplayDateFormate(dtDeposit.Rows[i]["Date"].ToString());
                        deposit.UploadType = dtDeposit.Rows[i]["UploadType"].ToString();
                        deposit.DepositeOffice = dtDeposit.Rows[i]["DepositeOffice"].ToString();
                        deposit.ConcernPerson = dtDeposit.Rows[i]["ConcernPerson"].ToString();
                        deposit.RecieptNo = !string.IsNullOrEmpty(dtDeposit.Rows[i]["RecieptNo"].ToString()) ? dtDeposit.Rows[i]["RecieptNo"].ToString() : "- - -";
                        deposit.BranchCode = dtDeposit.Rows[i]["BranchCode"].ToString();
                        deposit.AgentBankName = dtDeposit.Rows[i]["AgentBankName"].ToString();
                        deposit.AgentType = dtDeposit.Rows[i]["AgentType"].ToString();
                        deposit.AcceptedDateTime = dtDeposit.Rows[i]["AcceptedDateTime"].ToString();
                        deposit.DISTRID = dtDeposit.Rows[i]["DISTRID"].ToString();
                        deposit.InvoiceNo = dtDeposit.Rows[i]["InvoiceNo"].ToString();
                        deposit.IsMobile = !string.IsNullOrEmpty(dtDeposit.Rows[i]["IsMobile"].ToString()) ? Convert.ToBoolean(dtDeposit.Rows[i]["IsMobile"].ToString()) : false;
                        //deposit.OTPRefNo = dtDeposit.Rows[i]["OTPRefNo"].ToString();

                        depositList.Add(deposit);
                    }
                    result.PDList = depositList;
                    result.TotalCount = depositList.Count;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static PaymentDepositModel GetDepositDetailsByID(string deptid)
        {
            PaymentDepositModel deposit = new PaymentDepositModel();
            try
            {
                DataTable dtDeposit = AccountDatabase.GetDepositDetailsByID(deptid);
                if (dtDeposit != null && dtDeposit.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDeposit.Rows.Count; i++)
                    {
                        deposit.Counter = Convert.ToInt32(dtDeposit.Rows[i]["Counter"].ToString());
                        deposit.AgencyName = dtDeposit.Rows[i]["AgencyName"].ToString();
                        deposit.AgencyID = dtDeposit.Rows[i]["AgencyID"].ToString();
                        deposit.Amount = !string.IsNullOrEmpty(dtDeposit.Rows[i]["Amount"].ToString()) ? Convert.ToDecimal(dtDeposit.Rows[i]["Amount"].ToString()) : 0;
                        deposit.ModeOfPayment = dtDeposit.Rows[i]["ModeOfPayment"].ToString();
                        deposit.BankName = !string.IsNullOrEmpty(dtDeposit.Rows[i]["BankName"].ToString()) ? dtDeposit.Rows[i]["BankName"].ToString() : "- - -";
                        deposit.BranchName = dtDeposit.Rows[i]["BranchName"].ToString();
                        deposit.AccountNo = dtDeposit.Rows[i]["AccountNo"].ToString();
                        deposit.ChequeNo = !string.IsNullOrEmpty(dtDeposit.Rows[i]["ChequeNo"].ToString()) ? dtDeposit.Rows[i]["ChequeNo"].ToString() : "- - -";
                        deposit.ChequeDate = !string.IsNullOrEmpty(dtDeposit.Rows[i]["ChequeDate"].ToString()) ? dtDeposit.Rows[i]["ChequeDate"].ToString() : "- - -";
                        deposit.TransactionID = !string.IsNullOrEmpty(dtDeposit.Rows[i]["TransactionID"].ToString()) ? dtDeposit.Rows[i]["TransactionID"].ToString() : "- - -";
                        deposit.BankAreaCode = dtDeposit.Rows[i]["BankAreaCode"].ToString();
                        deposit.DepositCity = !string.IsNullOrEmpty(dtDeposit.Rows[i]["DepositCity"].ToString()) ? dtDeposit.Rows[i]["DepositCity"].ToString() : "- - -";
                        deposit.DepositeDate = dtDeposit.Rows[i]["DepositeDate"].ToString();
                        deposit.Remark = !string.IsNullOrEmpty(dtDeposit.Rows[i]["Remark"].ToString()) ? dtDeposit.Rows[i]["Remark"].ToString() : "- - -";
                        deposit.RemarkByAccounts = dtDeposit.Rows[i]["RemarkByAccounts"].ToString();
                        deposit.Status = dtDeposit.Rows[i]["Status"].ToString();
                        deposit.AccountID = dtDeposit.Rows[i]["AccountID"].ToString();
                        deposit.SalesExecID = dtDeposit.Rows[i]["SalesExecID"].ToString();
                        deposit.UpdatedDateTime = dtDeposit.Rows[i]["UpdatedDateTime"].ToString();
                        deposit.Date = UtilityClass.DisplayDateFormate(dtDeposit.Rows[i]["Date"].ToString());
                        deposit.UploadType = dtDeposit.Rows[i]["UploadType"].ToString();
                        deposit.DepositeOffice = dtDeposit.Rows[i]["DepositeOffice"].ToString();
                        deposit.ConcernPerson = dtDeposit.Rows[i]["ConcernPerson"].ToString();
                        deposit.RecieptNo = !string.IsNullOrEmpty(dtDeposit.Rows[i]["RecieptNo"].ToString()) ? dtDeposit.Rows[i]["RecieptNo"].ToString() : "- - -";
                        deposit.BranchCode = dtDeposit.Rows[i]["BranchCode"].ToString();
                        deposit.AgentBankName = dtDeposit.Rows[i]["AgentBankName"].ToString();
                        deposit.AgentType = dtDeposit.Rows[i]["AgentType"].ToString();
                        deposit.AcceptedDateTime = dtDeposit.Rows[i]["AcceptedDateTime"].ToString();
                        deposit.DISTRID = dtDeposit.Rows[i]["DISTRID"].ToString();
                        deposit.InvoiceNo = dtDeposit.Rows[i]["InvoiceNo"].ToString();
                        deposit.IsMobile = !string.IsNullOrEmpty(dtDeposit.Rows[i]["IsMobile"].ToString()) ? Convert.ToBoolean(dtDeposit.Rows[i]["IsMobile"].ToString()) : false;
                        //deposit.OTPRefNo = dtDeposit.Rows[i]["OTPRefNo"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return deposit;
        }
        public static int UpdateDepositDetails(string deptid, string agentId, string status, string type, string accountId, string remark)
        {
            return AccountDatabase.UpdateDepositDetails(deptid, agentId, status, type, accountId, remark);
        }
    }
}