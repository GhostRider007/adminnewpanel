﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminPanel.Helper
{
    public static class AdministrationHelper
    {
        public static bool ExecutiveLogin(string userName, string password, ref string msg)
        {
            bool issuccess = false;
            try
            {
                if (AdministrationDatabase.CheckIPAddressForLogin(userName, UtilityClass.GetLocalIPAddress()))
                {
                    DataTable dtExecutive = AdministrationDatabase.UserLogin_PP(userName, password);

                    if (dtExecutive != null && dtExecutive.Rows.Count > 0)
                    {
                        issuccess = true;
                        InitializeAgencySession(dtExecutive);
                    }
                    else
                    {
                        msg = "Userid/Password seems to be incorrect, Please try again?";
                    }
                }
            }
            catch (Exception ex)
            {
                issuccess = false;
                msg = ex.Message;
            }
            return issuccess;
        }
        private static void InitializeAgencySession(DataTable dtExecutive)
        {
            if (dtExecutive != null && dtExecutive.Rows.Count > 0)
            {
                ExecuRegister olu = new ExecuRegister();

                //olu.counter = !string.IsNullOrEmpty(dtExecutive.Rows[0]["counter"].ToString()) ? Convert.ToInt32(dtExecutive.Rows[0]["counter"].ToString()) : 0;
                olu.user_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["user_id"].ToString()) ? dtExecutive.Rows[0]["user_id"].ToString() : string.Empty;
                olu.password = !string.IsNullOrEmpty(dtExecutive.Rows[0]["password"].ToString()) ? dtExecutive.Rows[0]["password"].ToString() : string.Empty;
                olu.role_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["role_id"].ToString()) ? Convert.ToInt32(dtExecutive.Rows[0]["role_id"].ToString()) : 0;
                olu.role_type = !string.IsNullOrEmpty(dtExecutive.Rows[0]["role_type"].ToString()) ? dtExecutive.Rows[0]["role_type"].ToString() : string.Empty;

                olu.RedirectUrl = "/dashboard";
                if (olu.role_type.Trim().ToLower() == "admin")
                {
                    olu.user_type = "AD";
                    olu.type_id = "AD1";
                }
                else if (olu.role_type.Trim().ToLower() == "acc")
                {
                    olu.user_type = "AC";
                    olu.type_id = "AC1";
                }
                else if (olu.role_type.Trim().ToLower() == "exec")
                {
                    olu.user_type = "EC";
                    olu.type_id = "EC1";
                }
                else if (olu.role_type.Trim().ToLower() == "sales")
                {
                    olu.user_type = olu.role_type;
                    olu.type_id = olu.role_type;
                    olu.RedirectUrl = "SprReports/Admin/Agent_Details.aspx";
                }
                olu.TripExec = "D";
                olu.role_name = !string.IsNullOrEmpty(dtExecutive.Rows[0]["Role"].ToString()) ? dtExecutive.Rows[0]["Role"].ToString() : string.Empty;
                olu.DExport = StatusExport(olu.user_id, olu.role_name);
                olu.ModeTypeITZ = "WEB";
                olu.MchntKeyITZ = ConfigFile.MerchantKey;
                olu.SvcTypeITZ = "MERCHANTDB";
                olu.name = !string.IsNullOrEmpty(dtExecutive.Rows[0]["name"].ToString()) ? dtExecutive.Rows[0]["name"].ToString() : string.Empty;
                //olu.email_id = !string.IsNullOrEmpty(dtExecutive.Rows[0]["email_id"].ToString()) ? dtExecutive.Rows[0]["email_id"].ToString() : string.Empty;
                //olu.mobile_no = !string.IsNullOrEmpty(dtExecutive.Rows[0]["mobile_no"].ToString()) ? dtExecutive.Rows[0]["mobile_no"].ToString() : string.Empty;
                //olu.status = !string.IsNullOrEmpty(dtExecutive.Rows[0]["status"].ToString()) ? Convert.ToBoolean(dtExecutive.Rows[0]["status"].ToString()) : false;
                //olu.trip = !string.IsNullOrEmpty(dtExecutive.Rows[0]["trip"].ToString()) ? dtExecutive.Rows[0]["trip"].ToString() : string.Empty;
                olu.Branch = !string.IsNullOrEmpty(dtExecutive.Rows[0]["Branch"].ToString()) ? dtExecutive.Rows[0]["Branch"].ToString() : string.Empty;

                List<ExecuRegister> lu = new List<ExecuRegister>();
                lu.Add(olu);
                HttpContext.Current.Session["executive"] = lu;
            }
        }
        public static bool IsExecutiveLogin(ref ExecuRegister lu)
        {
            List<ExecuRegister> objLoginUser = new List<ExecuRegister>();
            bool idsuccess = IsExecutiveLoginSuccess(ref objLoginUser);
            if (objLoginUser != null && objLoginUser.Count > 0)
            {
                lu = objLoginUser[0];
            }
            return idsuccess;
        }
        public static bool IsExecutiveLoginSuccess(ref List<ExecuRegister> loginUserList)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["executive"] != null)
                {
                    loginUserList = (List<ExecuRegister>)HttpContext.Current.Session["executive"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }
        public static bool LogoutExecutive()
        {
            HttpContext.Current.Session.Remove("executive");
            return true;
        }
        public static List<MenuList> ExecMenuList(int roleid)
        {
            List<MenuList> menuList = new List<MenuList>();
            try
            {
                DataTable dtMenu = AdministrationDatabase.GetMenuList(roleid);
                if (dtMenu != null && dtMenu.Rows.Count > 0)
                {
                    for (int i = 0; i < dtMenu.Rows.Count; i++)
                    {
                        MenuList menu = new MenuList();
                        menu.page_id = Convert.ToInt32(dtMenu.Rows[i]["page_id"].ToString());
                        menu.Page_name = dtMenu.Rows[i]["Page_name"].ToString();
                        menu.Page_url = dtMenu.Rows[i]["Page_url"].ToString();
                        menu.Is_Parent_Page = dtMenu.Rows[i]["Is_Parent_Page"].ToString();
                        menu.MainMenu = dtMenu.Rows[i]["Is_Parent_Page"].ToString().Trim() == "Y" ? true : false;
                        menu.Root_page_id = Convert.ToInt32(dtMenu.Rows[i]["Root_page_id"].ToString());
                        menuList.Add(menu);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return menuList.OrderBy(p => p.Page_name).ToList();
        }
        private static bool StatusExport(string userid, string role)
        {
            bool isactive = false;
            try
            {
                DataTable dtExport = AdministrationDatabase.StatusExport(userid, role);
                if (dtExport != null && dtExport.Rows.Count > 0)
                {
                    isactive = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return isactive;
        }
    }
}