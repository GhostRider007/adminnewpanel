﻿using AdminPanel.DataBase;
using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace AdminPanel.Helper
{
    public static class ProfileHelper
    {
        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
            return ProfileDatabase.ChangePassword(userid, oldpassword, newpassword);
        }
        #endregion
        #region[AgencyTDS]
        internal static string AgencyList_ddl()
        {
            string result = "<option value=\"\">Select Agency</option>";
            try
            {
                DataTable dt = ProfileDatabase.AgencyList();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            result = result + "<option value=\"" + dt.Rows[i]["User_Id"].ToString() + "\">" + dt.Rows[i]["Agency_Name"].ToString() + "</option>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        internal static AgencyDetail GetAgency(string userid)
        {
            AgencyDetail model = new AgencyDetail();
            try
            {
                DataTable dt = ProfileDatabase.AgencyList();
                var rows = dt.AsEnumerable().Where(myRow => myRow.Field<string>("User_Id") == userid);

                foreach (DataRow row in rows)
                {
                    model.Counter = Convert.ToInt32(row["Counter"].ToString());
                    model.Agency_Name = row["Agency_Name"].ToString();
                    model.User_Id = row["User_Id"].ToString();
                    model.Agent_Type = row["Agent_Type"].ToString();
                    model.TDS = row["TDS"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        internal static bool UpdateAgencyTDS(int id, string tds)
        {
            return ProfileDatabase.UpdateAgencyTDS(id, tds);
        }
        #endregion
        public static Agency GetAgencyList(Agency model)
        {
            try
            {
                DataTable dtAgency = ProfileDatabase.GetAgencyDetailsDynamic(model);
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    List<Agency> agencyList = new List<Agency>();
                    for (int i = 0; i < dtAgency.Rows.Count; i++)
                    {
                        Agency agency = new Agency();
                        agency.Counter = !string.IsNullOrEmpty(dtAgency.Rows[i]["Counter"].ToString()) ? dtAgency.Rows[i]["Counter"].ToString() : "- - -";
                        agency.User_Id = !string.IsNullOrEmpty(dtAgency.Rows[i]["User_Id"].ToString()) ? dtAgency.Rows[i]["User_Id"].ToString() : "- - -";
                        agency.AgencyId = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgencyId"].ToString()) ? dtAgency.Rows[i]["AgencyId"].ToString() : "- - -";
                        agency.Agency_Name = !string.IsNullOrEmpty(dtAgency.Rows[i]["Agency_Name"].ToString()) ? dtAgency.Rows[i]["Agency_Name"].ToString() : "- - -";
                        agency.Crd_Limit = !string.IsNullOrEmpty(dtAgency.Rows[i]["Crd_Limit"].ToString()) ? dtAgency.Rows[i]["Crd_Limit"].ToString() : "- - -";
                        agency.Balance = !string.IsNullOrEmpty(dtAgency.Rows[i]["Balance"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[i]["Balance"].ToString())) : "- - -";
                        agency.DueAmount = !string.IsNullOrEmpty(dtAgency.Rows[i]["DueAmount"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[i]["DueAmount"].ToString())) : "- - -";
                        agency.AgentLimit = !string.IsNullOrEmpty(dtAgency.Rows[i]["AgentLimit"].ToString()) ? String.Format("{0:0.00}", Convert.ToDecimal(dtAgency.Rows[i]["AgentLimit"].ToString())) : "- - -";
                        agency.Crd_Trns_Date = !string.IsNullOrEmpty(dtAgency.Rows[i]["Crd_Trns_Date"].ToString()) ? UtilityClass.DisplayDateFormate(dtAgency.Rows[i]["Crd_Trns_Date"].ToString()) : "- - -";
                        agency.Mobile = !string.IsNullOrEmpty(dtAgency.Rows[i]["Mobile"].ToString()) ? dtAgency.Rows[i]["Mobile"].ToString() : "- - -";
                        agency.Email = !string.IsNullOrEmpty(dtAgency.Rows[i]["Email"].ToString()) ? dtAgency.Rows[i]["Email"].ToString() : "- - -";
                        agency.Agent_Type = !string.IsNullOrEmpty(dtAgency.Rows[i]["Agent_Type"].ToString()) ? dtAgency.Rows[i]["Agent_Type"].ToString() : "- - -";
                        agency.Title = !string.IsNullOrEmpty(dtAgency.Rows[i]["Title"].ToString()) ? dtAgency.Rows[i]["Title"].ToString() : "- - -";
                        agency.Fname = !string.IsNullOrEmpty(dtAgency.Rows[i]["Fname"].ToString()) ? dtAgency.Rows[i]["Fname"].ToString() : "- - -";
                        agency.Lname = !string.IsNullOrEmpty(dtAgency.Rows[i]["Lname"].ToString()) ? dtAgency.Rows[i]["Lname"].ToString() : "- - -";
                        agency.Name = !string.IsNullOrEmpty(dtAgency.Rows[i]["Name"].ToString()) ? dtAgency.Rows[i]["Name"].ToString() : "- - -";
                        agency.Address = !string.IsNullOrEmpty(dtAgency.Rows[i]["Address"].ToString()) ? dtAgency.Rows[i]["Address"].ToString() : "- - -";
                        agency.City = !string.IsNullOrEmpty(dtAgency.Rows[i]["City"].ToString()) ? dtAgency.Rows[i]["City"].ToString() : "- - -";
                        agency.State = !string.IsNullOrEmpty(dtAgency.Rows[i]["State"].ToString()) ? dtAgency.Rows[i]["State"].ToString() : "- - -";
                        agency.Country = !string.IsNullOrEmpty(dtAgency.Rows[i]["Country"].ToString()) ? dtAgency.Rows[i]["Country"].ToString() : "- - -";
                        agency.zipcode = !string.IsNullOrEmpty(dtAgency.Rows[i]["zipcode"].ToString()) ? dtAgency.Rows[i]["zipcode"].ToString() : "- - -";
                        agency.Phone = !string.IsNullOrEmpty(dtAgency.Rows[i]["Phone"].ToString()) ? dtAgency.Rows[i]["Phone"].ToString() : "- - -";
                        agency.Alt_Email = !string.IsNullOrEmpty(dtAgency.Rows[i]["Alt_Email"].ToString()) ? dtAgency.Rows[i]["Alt_Email"].ToString() : "- - -";
                        agency.Fax_no = !string.IsNullOrEmpty(dtAgency.Rows[i]["Fax_no"].ToString()) ? dtAgency.Rows[i]["Fax_no"].ToString() : "- - -";
                        agency.Website = !string.IsNullOrEmpty(dtAgency.Rows[i]["Website"].ToString()) ? dtAgency.Rows[i]["Website"].ToString() : "- - -";
                        agency.NamePanCard = !string.IsNullOrEmpty(dtAgency.Rows[i]["NamePanCard"].ToString()) ? dtAgency.Rows[i]["NamePanCard"].ToString() : "- - -";
                        agency.PanNo = !string.IsNullOrEmpty(dtAgency.Rows[i]["PanNo"].ToString()) ? dtAgency.Rows[i]["PanNo"].ToString() : "- - -";
                        agency.IrctcId = !string.IsNullOrEmpty(dtAgency.Rows[i]["IrctcId"].ToString()) ? dtAgency.Rows[i]["IrctcId"].ToString() : "- - -";
                        agency.Status = !string.IsNullOrEmpty(dtAgency.Rows[i]["Status"].ToString()) ? dtAgency.Rows[i]["Status"].ToString() : "- - -";
                        agency.Stax_no = !string.IsNullOrEmpty(dtAgency.Rows[i]["Stax_no"].ToString()) ? dtAgency.Rows[i]["Stax_no"].ToString() : "- - -";
                        agency.Remark = !string.IsNullOrEmpty(dtAgency.Rows[i]["Remark"].ToString()) ? dtAgency.Rows[i]["Remark"].ToString() : "- - -";
                        agency.Sec_Qes = !string.IsNullOrEmpty(dtAgency.Rows[i]["Sec_Qes"].ToString()) ? dtAgency.Rows[i]["Sec_Qes"].ToString() : "- - -";
                        agency.Sec_Ans = !string.IsNullOrEmpty(dtAgency.Rows[i]["Sec_Ans"].ToString()) ? dtAgency.Rows[i]["Sec_Ans"].ToString() : "- - -";
                        agency.GSTNO = !string.IsNullOrEmpty(dtAgency.Rows[i]["GSTNO"].ToString()) ? dtAgency.Rows[i]["GSTNO"].ToString() : "- - -";
                        agency.Distr = !string.IsNullOrEmpty(dtAgency.Rows[i]["Distr"].ToString()) ? dtAgency.Rows[i]["Distr"].ToString() : "- - -";
                        agency.DistrAgencyID = !string.IsNullOrEmpty(dtAgency.Rows[i]["DistrAgencyID"].ToString()) ? dtAgency.Rows[i]["DistrAgencyID"].ToString() : "- - -";
                        agency.TDS = !string.IsNullOrEmpty(dtAgency.Rows[i]["TDS"].ToString()) ? dtAgency.Rows[i]["TDS"].ToString() : "- - -";
                        agency.ag_logo = !string.IsNullOrEmpty(dtAgency.Rows[i]["ag_logo"].ToString()) ? dtAgency.Rows[i]["ag_logo"].ToString() : "- - -";
                        agency.Agent_status = !string.IsNullOrEmpty(dtAgency.Rows[i]["Agent_status"].ToString()) ? dtAgency.Rows[i]["Agent_status"].ToString() : "- - -";
                        agency.Online_Tkt = !string.IsNullOrEmpty(dtAgency.Rows[i]["Online_Tkt"].ToString()) ? dtAgency.Rows[i]["Online_Tkt"].ToString() : "- - -";
                        agency.timestamp_create = !string.IsNullOrEmpty(dtAgency.Rows[i]["timestamp_create"].ToString()) ? UtilityClass.DisplayDateFormate(dtAgency.Rows[i]["timestamp_create"].ToString()) : "- - -";
                        agency.ExmptTDS = !string.IsNullOrEmpty(dtAgency.Rows[i]["ExmptTDS"].ToString()) ? dtAgency.Rows[i]["ExmptTDS"].ToString() : "- - -";
                        agency.ExmptTdsLimit = !string.IsNullOrEmpty(dtAgency.Rows[i]["ExmptTdsLimit"].ToString()) ? dtAgency.Rows[i]["ExmptTdsLimit"].ToString() : "- - -";
                        agency.SalesExecID = !string.IsNullOrEmpty(dtAgency.Rows[i]["SalesExecID"].ToString()) ? dtAgency.Rows[i]["SalesExecID"].ToString() : "- - -";
                        agency.IsWhiteLabel = !string.IsNullOrEmpty(dtAgency.Rows[i]["IsWhiteLabel"].ToString()) ? dtAgency.Rows[i]["IsWhiteLabel"].ToString() : "- - -";

                        agencyList.Add(agency);
                    }
                    model.AgencyList = agencyList;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return model;
        }
        public static Dictionary<string, string> GetAgencyTypeList()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtAgencyType = ProfileDatabase.GetAgencyType();
                if (dtAgencyType != null && dtAgencyType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgencyType.Rows.Count; i++)
                    {
                        pairList.Add(dtAgencyType.Rows[i]["GroupType"].ToString(), dtAgencyType.Rows[i]["GroupType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetSalesRef()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtSalesRef = ProfileDatabase.GetSalesRef();
                if (dtSalesRef != null && dtSalesRef.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSalesRef.Rows.Count; i++)
                    {
                        pairList.Add(dtSalesRef.Rows[i]["EmailId"].ToString(), dtSalesRef.Rows[i]["EmailId"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static SingleAgency_SP GetAgencyDetailsByUserId(string userId)
        {
            SingleAgency_SP agencyDel = new SingleAgency_SP();
            try
            {
                DataTable dtAgency = ProfileDatabase.GetAgencyDetailsByUserId(userId);
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgency.Rows.Count; i++)
                    {
                        agencyDel.Counter = dtAgency.Rows[i]["Counter"].ToString();
                        agencyDel.Title = dtAgency.Rows[i]["Title"].ToString();
                        agencyDel.Fname = dtAgency.Rows[i]["Fname"].ToString();
                        agencyDel.Lname = dtAgency.Rows[i]["Lname"].ToString();
                        agencyDel.Name = dtAgency.Rows[i]["Name"].ToString();
                        agencyDel.Address = dtAgency.Rows[i]["Address"].ToString();
                        agencyDel.City = dtAgency.Rows[i]["City"].ToString();
                        agencyDel.State = dtAgency.Rows[i]["State"].ToString();
                        agencyDel.Country = dtAgency.Rows[i]["Country"].ToString();
                        agencyDel.zipcode = dtAgency.Rows[i]["zipcode"].ToString();
                        agencyDel.Phone = dtAgency.Rows[i]["Phone"].ToString();
                        agencyDel.Mobile = dtAgency.Rows[i]["Mobile"].ToString();
                        agencyDel.Email = dtAgency.Rows[i]["Email"].ToString();
                        agencyDel.Alt_Email = dtAgency.Rows[i]["Alt_Email"].ToString();
                        agencyDel.Fax_no = dtAgency.Rows[i]["Fax_no"].ToString();
                        agencyDel.Agency_Name = dtAgency.Rows[i]["Agency_Name"].ToString();
                        agencyDel.Website = dtAgency.Rows[i]["Website"].ToString();
                        agencyDel.NamePanCard = dtAgency.Rows[i]["NamePanCard"].ToString();
                        agencyDel.PanNo = dtAgency.Rows[i]["PanNo"].ToString();
                        agencyDel.Status = dtAgency.Rows[i]["Status"].ToString();
                        agencyDel.Stax_no = dtAgency.Rows[i]["Stax_no"].ToString();
                        agencyDel.Remark = dtAgency.Rows[i]["Remark"].ToString();
                        agencyDel.Sec_Qes = dtAgency.Rows[i]["Sec_Qes"].ToString();
                        agencyDel.Sec_Ans = dtAgency.Rows[i]["Sec_Ans"].ToString();
                        agencyDel.User_Id = dtAgency.Rows[i]["User_Id"].ToString();
                        agencyDel.PWD = dtAgency.Rows[i]["PWD"].ToString();
                        agencyDel.Agent_Type = dtAgency.Rows[i]["Agent_Type"].ToString();
                        agencyDel.ddlAgent_Type = agencyDel.Agent_Type;
                        agencyDel.Crd_Limit = dtAgency.Rows[i]["Crd_Limit"].ToString();
                        agencyDel.Crd_Trns_Date = dtAgency.Rows[i]["Crd_Trns_Date"].ToString();
                        agencyDel.Distr = dtAgency.Rows[i]["Distr"].ToString();
                        agencyDel.ddlDistr = agencyDel.Distr;
                        agencyDel.ag_logo = dtAgency.Rows[i]["ag_logo"].ToString();
                        agencyDel.Agent_status = dtAgency.Rows[i]["Agent_status"].ToString();
                        agencyDel.TDS = dtAgency.Rows[i]["TDS"].ToString();
                        agencyDel.Online_Tkt = dtAgency.Rows[i]["Online_Tkt"].ToString();
                        agencyDel.timestamp_create = dtAgency.Rows[i]["timestamp_create"].ToString();
                        agencyDel.ExmptTDS = dtAgency.Rows[i]["ExmptTDS"].ToString();
                        agencyDel.ExmptTdsLimit = dtAgency.Rows[i]["ExmptTdsLimit"].ToString();
                        agencyDel.SalesExecID = dtAgency.Rows[i]["SalesExecID"].ToString();
                        agencyDel.IsCorp = dtAgency.Rows[i]["IsCorp"].ToString();
                        agencyDel.IsPWD = dtAgency.Rows[i]["IsPWD"].ToString();
                        agencyDel.OTPLoginStatus = Convert.ToBoolean(dtAgency.Rows[i]["OTPLoginStatus"].ToString());
                        agencyDel.PasswordChangeDate = dtAgency.Rows[i]["PasswordChangeDate"].ToString();
                        agencyDel.PasswordExpMsg = Convert.ToBoolean(dtAgency.Rows[i]["PasswordExpMsg"].ToString());
                        agencyDel.GSTNO = dtAgency.Rows[i]["GSTNO"].ToString();
                        agencyDel.GST_Company_Name = dtAgency.Rows[i]["GST_Company_Name"].ToString();
                        agencyDel.GST_Company_Address = dtAgency.Rows[i]["GST_Company_Address"].ToString();
                        agencyDel.GST_PhoneNo = dtAgency.Rows[i]["GST_PhoneNo"].ToString();
                        agencyDel.GST_Email = dtAgency.Rows[i]["GST_Email"].ToString();
                        agencyDel.Is_GST_Apply = dtAgency.Rows[i]["Is_GST_Apply"].ToString();
                        agencyDel.GSTRemark = dtAgency.Rows[i]["GSTRemark"].ToString();
                        agencyDel.GST_City = dtAgency.Rows[i]["GST_City"].ToString();
                        agencyDel.GST_State = dtAgency.Rows[i]["GST_State"].ToString();
                        agencyDel.GST_Pincode = dtAgency.Rows[i]["GST_Pincode"].ToString();
                        agencyDel.IrctcId = dtAgency.Rows[i]["IrctcId"].ToString();
                        agencyDel.irctctype = dtAgency.Rows[i]["irctctype"].ToString();
                        agencyDel.irctcamt = dtAgency.Rows[i]["irctcamt"].ToString();
                        agencyDel.irctc_duepaydate = dtAgency.Rows[i]["irctc_duepaydate"].ToString();
                        agencyDel.GSTAddress = dtAgency.Rows[i]["GSTAddress"].ToString();
                        agencyDel.GST_State_Code = dtAgency.Rows[i]["GST_State_Code"].ToString();
                        agencyDel.AgentLimit = dtAgency.Rows[i]["AgentLimit"].ToString();
                        agencyDel.DueAmount = dtAgency.Rows[i]["DueAmount"].ToString();
                        agencyDel.VirtualCreditLimit = dtAgency.Rows[i]["VirtualCreditLimit"].ToString();
                        agencyDel.AgentLimitTrnsDate = dtAgency.Rows[i]["AgentLimitTrnsDate"].ToString();
                        agencyDel.DueAmtTrnsDate = dtAgency.Rows[i]["DueAmtTrnsDate"].ToString();
                        agencyDel.VirtualFromDate = dtAgency.Rows[i]["VirtualFromDate"].ToString();
                        agencyDel.VirtualToDate = dtAgency.Rows[i]["VirtualToDate"].ToString();
                        agencyDel.VirtualCreditTrnsDate = dtAgency.Rows[i]["VirtualCreditTrnsDate"].ToString();
                        agencyDel.AgencyId = dtAgency.Rows[i]["AgencyId"].ToString();
                        agencyDel.IsWhiteLabel = dtAgency.Rows[i]["IsWhiteLabel"].ToString();
                        agencyDel.Balance = dtAgency.Rows[i]["Balance"].ToString();
                        agencyDel.LimitExpiryDate = dtAgency.Rows[i]["LimitExpiryDate"].ToString();
                        agencyDel.FixedLimit = dtAgency.Rows[i]["FixedLimit"].ToString();
                        agencyDel.FixedLimitTrnsDate = dtAgency.Rows[i]["FixedLimitTrnsDate"].ToString();
                        agencyDel.TempLimit = dtAgency.Rows[i]["TempLimit"].ToString();
                        agencyDel.TempLimitTrnsDate = dtAgency.Rows[i]["TempLimitTrnsDate"].ToString();
                        agencyDel.TempLimitFromDate = dtAgency.Rows[i]["TempLimitFromDate"].ToString();
                        agencyDel.TempLimitToDate = dtAgency.Rows[i]["TempLimitToDate"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return agencyDel;
        }
        public static Dictionary<string, string> GetDistrdetails()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtDistr = ProfileDatabase.GetDistrdetails();
                if (dtDistr != null && dtDistr.Rows.Count > 0)
                {
                    for (int i = 0; i < dtDistr.Rows.Count; i++)
                    {
                        pairList.Add(dtDistr.Rows[i]["Agency_Name"].ToString(), dtDistr.Rows[i]["User_Id"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetAgentGroupType()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtAgentType = ProfileDatabase.GetAgentGroupType();
                if (dtAgentType != null && dtAgentType.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAgentType.Rows.Count; i++)
                    {
                        pairList.Add(dtAgentType.Rows[i]["GroupType"].ToString(), dtAgentType.Rows[i]["GroupType"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetStateList()
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtStateList = ProfileDatabase.GetStateList();
                if (dtStateList != null && dtStateList.Rows.Count > 0)
                {
                    for (int i = 0; i < dtStateList.Rows.Count; i++)
                    {
                        pairList.Add(dtStateList.Rows[i]["Name"].ToString(), dtStateList.Rows[i]["Code"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static Dictionary<string, string> GetCityList(string stateCode)
        {
            Dictionary<string, string> pairList = new Dictionary<string, string>();
            try
            {
                DataTable dtCityList = ProfileDatabase.GetCityList(stateCode);
                if (dtCityList != null && dtCityList.Rows.Count > 0)
                {
                    for (int i = 0; i < dtCityList.Rows.Count; i++)
                    {
                        pairList.Add(dtCityList.Rows[i]["CITY"].ToString(), dtCityList.Rows[i]["CITY"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pairList;
        }
        public static int UpdateAgencyDetail(SingleAgency_SP model)
        {
            return ProfileDatabase.UpdateAgencyDetail(model);
        }
        public static int InsertAgencyRegistration(AgencyDetail model)
        {
            return ProfileDatabase.InsertAgencyRegistration(model);
        }
    }
}