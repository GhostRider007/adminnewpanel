﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AdminPanel.DataBase;
using AdminPanel.Models;

namespace AdminPanel.Helper
{
    public static class AutoCompleteHelper
    {
        public static List<AutoAgency> GetAgencyAutoSearch(string inputPara, string userType, string distr)
        {
            List<AutoAgency> agencyList = new List<AutoAgency>();
            try
            {
                DataTable dtAgencyAuto = AutoCompleteDatabase.GetAgencyAutoSearch(inputPara, userType, distr);
                if (dtAgencyAuto != null && dtAgencyAuto.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtAgencyAuto.Rows.Count; i++)
                    {
                        AutoAgency agency = new AutoAgency();
                        agency.Agency_Name = dtAgencyAuto.Rows[i]["Agency_Name"].ToString();
                        agency.User_Id = dtAgencyAuto.Rows[i]["User_Id"].ToString();
                        agencyList.Add(agency);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return agencyList;
        }
        public static List<AutoAirline> GetAirlineSearch(string inputPara)
        {
            List<AutoAirline> airlineList = new List<AutoAirline>();
            try
            {
                DataTable dtAirline = AutoCompleteDatabase.GetAirlineSearch(inputPara);
                if (dtAirline != null && dtAirline.Rows.Count > 0)
                {
                    for (int i = 0; i <= dtAirline.Rows.Count; i++)
                    {
                        AutoAirline agency = new AutoAirline();
                        agency.AirlineName = dtAirline.Rows[i]["AL_Name"].ToString();
                        agency.AirlineCode = dtAirline.Rows[i]["AL_Code"].ToString();
                        airlineList.Add(agency);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return airlineList;
        }
        public static AutoNotification GetNotificationDetails()
        {
            AutoNotification notification = new AutoNotification();
            try
            {
                DataSet dsNotification = AutoCompleteDatabase.GetNotificationDetails();
                if (dsNotification != null)
                {
                    DataTable dtTicketCancel = dsNotification.Tables[0];
                    if (dtTicketCancel != null && dtTicketCancel.Rows.Count > 0)
                    {
                        List<AutoNotification> ticketCan = new List<AutoNotification>();
                        for (int i = 0; i < dtTicketCancel.Rows.Count; i++)
                        {
                            AutoNotification objNot = new AutoNotification();
                            objNot.Agency_Name = dtTicketCancel.Rows[i]["Agency_Name"].ToString();
                            objNot.OrderId = dtTicketCancel.Rows[i]["OrderId"].ToString();
                            ticketCan.Add(objNot);
                        }
                        notification.TicketCancelList = ticketCan;
                    }

                    DataTable dtTicketHold = dsNotification.Tables[1];
                    if (dtTicketHold != null && dtTicketHold.Rows.Count > 0)
                    {
                        List<AutoNotification> ticketHold = new List<AutoNotification>();
                        for (int i = 0; i < dtTicketHold.Rows.Count; i++)
                        {
                            AutoNotification objHold = new AutoNotification();
                            objHold.Agency_Name = dtTicketHold.Rows[i]["AgencyName"].ToString();
                            objHold.OrderId = dtTicketHold.Rows[i]["OrderId"].ToString();
                            ticketHold.Add(objHold);
                        }
                        notification.TicketHoldList = ticketHold;
                    }

                    DataTable dtTicketReissue = dsNotification.Tables[2];
                    if (dtTicketReissue != null && dtTicketReissue.Rows.Count > 0)
                    {
                        List<AutoNotification> ticketReIssue = new List<AutoNotification>();
                        for (int i = 0; i < dtTicketReissue.Rows.Count; i++)
                        {
                            AutoNotification objReIss = new AutoNotification();
                            objReIss.Agency_Name = dtTicketReissue.Rows[i]["Agency_Name"].ToString();
                            objReIss.OrderId = dtTicketReissue.Rows[i]["OrderId"].ToString();
                            ticketReIssue.Add(objReIss);
                        }
                        notification.TicketReissueList = ticketReIssue;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return notification;
        }
    }
}