﻿using AdminPanel.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AdminPanel.DataBase
{
    public static class FlightSettingDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[FareTypeMaster]
        internal static DataTable FareTypeMasterList()
        {
            try
            {
                ObjDataTable = GetRecord_FareTypeMaster(0, "", "ALL");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        internal static bool UpdateFareTypeMaster(FareTypeMaster model)
        {
            try
            {
                bool isSuccess = InsertAndUpdate_FareTypeMaster(model.Id,model.FareType,model.DisplayName,model.Remark, "update",model.UpdatedBy) ;
                if (isSuccess) 
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        private static DataTable GetRecord_FareTypeMaster(int Id, string FareType, string ActionType)
        {
            try
            {
                Command = new SqlCommand("USP_FARETYPEMASTER_UPADTE", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Id", Id);
                Command.Parameters.AddWithValue("@FareType", FareType);
                Command.Parameters.AddWithValue("@Action", ActionType);
                Command.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                Command.Parameters["@Msg"].Direction = ParameterDirection.Output;
                Adapter = new SqlDataAdapter();

                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "FareTypeMaster");
                ObjDataTable = ObjDataSet.Tables["FareTypeMaster"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return ObjDataTable;
        }
        private static bool InsertAndUpdate_FareTypeMaster(int Id, string FareType, string DisplayName, string Remark, string ActionType, string ActionBy)
        {
            //string ActionBy = Convert.ToString(Session["UID"]);
            try
            {
                Command = new SqlCommand("USP_FARETYPEMASTER_UPADTE", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Id", Id);
                Command.Parameters.AddWithValue("@FareType", FareType);
                Command.Parameters.AddWithValue("@DisplayName", DisplayName);
                Command.Parameters.AddWithValue("@Remark", Remark);
                Command.Parameters.AddWithValue("@ActionBy", ActionBy);
                Command.Parameters.AddWithValue("@Action", ActionType);
                Command.Parameters.Add("@Msg", SqlDbType.VarChar, 100);
                Command.Parameters["@Msg"].Direction = ParameterDirection.Output;
                OpenConnection(conString);
                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                string msgout = Command.Parameters["@Msg"].Value.ToString();

                if (result > 0) 
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion
    }
}