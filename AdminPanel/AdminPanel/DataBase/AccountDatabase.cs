﻿using AdminPanel.Models;
using System;
using System.Data;
using System.Data.SqlClient;

namespace AdminPanel.DataBase
{
    public static class AccountDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion
        public static DataTable GetLedgerDetails(string userType, string loginID, LedgerModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetLedgerDetail_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("usertype", userType);
                Command.Parameters.AddWithValue("LoginID", loginID);
                Command.Parameters.AddWithValue("FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("BookingType", !string.IsNullOrEmpty(model.FilterBookingType) ? model.FilterBookingType : "");
                Command.Parameters.AddWithValue("SearchType", !string.IsNullOrEmpty(model.FilterSearchType) ? model.FilterSearchType : "Own");
                Command.Parameters.AddWithValue("PaymentMode", !string.IsNullOrEmpty(model.FilterPaymentMode) ? model.FilterPaymentMode : "All");
                Command.Parameters.AddWithValue("TransType", !string.IsNullOrEmpty(model.FilterTransType) ? model.FilterTransType : "");

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "LedgerDetail");
                ObjDataTable = ObjDataSet.Tables["LedgerDetail"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetBookingTypeFromLedger()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("usp_Get_Distinct_BookingTypeFromLedger_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "BookingTypeFromLedger");
                ObjDataTable = ObjDataSet.Tables["BookingTypeFromLedger"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetTransTypeFromLedger()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("usp_Get_Distinct_TransTypeFromLedger", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "TransTypeFromLedger");
                ObjDataTable = ObjDataSet.Tables["TransTypeFromLedger"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetSaleRejister(string userType, string loginID, LedgerModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("IntSelectInvoiceDetails_New", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@usertype", userType);
                Command.Parameters.AddWithValue("@LoginID", loginID);
                Command.Parameters.AddWithValue("@FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("@OderId", !string.IsNullOrEmpty(model.FilterOderId) ? model.FilterOderId : "");
                Command.Parameters.AddWithValue("@PNR", !string.IsNullOrEmpty(model.FilterPNR) ? model.FilterPNR : "");
                Command.Parameters.AddWithValue("@TicketNo", !string.IsNullOrEmpty(model.FilterTicketNo) ? model.FilterTicketNo : "");
                Command.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("@Airline", !string.IsNullOrEmpty(model.FilterAirline) ? model.FilterAirline : "");
                Command.Parameters.AddWithValue("@Trip", "D");
                Command.Parameters.AddWithValue("@ProjectID", !string.IsNullOrEmpty(model.FilterProjectID) ? model.FilterProjectID : "");
                Command.Parameters.AddWithValue("@PaymentMode", !string.IsNullOrEmpty(model.FilterPaymentMode) ? model.FilterPaymentMode : "");
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "SelectInvoiceDetail");
                ObjDataTable = ObjDataSet.Tables["SelectInvoiceDetail"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetUnflowReport(string userType, string loginID, LedgerModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("SP_GetOutFlowInvoice", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@usertype", userType);
                Command.Parameters.AddWithValue("@LoginID", loginID);
                Command.Parameters.AddWithValue("@FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("@OderId", !string.IsNullOrEmpty(model.FilterOderId) ? model.FilterOderId : "");
                Command.Parameters.AddWithValue("@PNR", !string.IsNullOrEmpty(model.FilterPNR) ? model.FilterPNR : "");
                Command.Parameters.AddWithValue("@TicketNo", !string.IsNullOrEmpty(model.FilterTicketNo) ? model.FilterTicketNo : "");
                Command.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("@Airline", !string.IsNullOrEmpty(model.FilterAirline) ? model.FilterAirline : "");
                Command.Parameters.AddWithValue("@Trip", !string.IsNullOrEmpty(model.FilterTrip) ? model.FilterTrip : "");
                Command.Parameters.AddWithValue("@ProjectID", !string.IsNullOrEmpty(model.FilterProjectID) ? model.FilterProjectID : "");
                Command.Parameters.AddWithValue("@PaymentMode", !string.IsNullOrEmpty(model.FilterPaymentMode) ? model.FilterPaymentMode : "");
                Command.Parameters.AddWithValue("@Departfrom", !string.IsNullOrEmpty(model.FilterDepartfrom) ? model.FilterDepartfrom : "");
                Command.Parameters.AddWithValue("@DepartTo", !string.IsNullOrEmpty(model.FilterDepartTo) ? model.FilterDepartTo : "");
                Command.Parameters.AddWithValue("@SalesPerson", !string.IsNullOrEmpty(model.FilterSalesPerson) ? model.FilterSalesPerson : "");
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "GetOutFlowInvoice");
                ObjDataTable = ObjDataSet.Tables["GetOutFlowInvoice"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetCreditLimit(string userType, string loginID, LedgerModel model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GET_CREDITLIMIT_HISTORY", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@FormDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");
                Command.Parameters.AddWithValue("@ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("@AgentId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CREDITLIMIT_HISTORY");
                ObjDataTable = ObjDataSet.Tables["CREDITLIMIT_HISTORY"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetDepositStatusDetail(string status, string agencId = "", string accountId = "")
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetDepositStatusDetails", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@Status", status);
                Command.Parameters.AddWithValue("@AgencyID", agencId);
                Command.Parameters.AddWithValue("@AccountID", accountId);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "DepositStatus");
                ObjDataTable = ObjDataSet.Tables["DepositStatus"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static DataTable GetDepositDetailsByID(string deptid)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetDepositeDetailsByID", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("@ID", deptid);
                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "DepositStatus");
                ObjDataTable = ObjDataSet.Tables["DepositStatus"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        public static int UpdateDepositDetails(string deptid, string agentId, string status, string type, string accountId, string remark)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("UpdateDepositDetails", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("ID", deptid);
                Command.Parameters.AddWithValue("AgentID", agentId);
                Command.Parameters.AddWithValue("Status", status);
                Command.Parameters.AddWithValue("Type", type);
                Command.Parameters.AddWithValue("AccountID", accountId);
                Command.Parameters.AddWithValue("Rmk", remark);

                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                return result;
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return 0;
        }
    }
}