﻿using AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdminPanel.DataBase
{
    public class ProfileDatabase
    {
        #region [Common : Connect to database]                
        private static SqlConnection conString = new SqlConnection(ConfigFile.DatabaseConnectionString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[Change Password]
        internal static bool ChangePassword(string userid, string oldpassword, string newpassword)
        {
            try
            {
                Command = new SqlCommand("select * from ExecuRegister  where user_id='" + userid + "'", conString);
                Command.CommandType = CommandType.Text;

                OpenConnection(conString);
                SqlDataReader rd = Command.ExecuteReader();
                bool doesExist = false;
                while (rd.Read())
                {
                    if (oldpassword == rd["password"].ToString())
                    {
                        doesExist = true;
                    }
                }
                rd.Close();
                CloseConnection(conString);
                Command.Dispose();


                if (doesExist)
                {
                    Command = new SqlCommand("usp_changePassword_PP", conString);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@userID", SqlDbType.NVarChar, 500).Value = userid;
                    Command.Parameters.Add("@oldPassword", SqlDbType.NVarChar, 100).Value = oldpassword;
                    Command.Parameters.Add("@Password", SqlDbType.NVarChar, 500).Value = newpassword;

                    OpenConnection(conString);
                    int result = Convert.ToInt32(Command.ExecuteScalar());
                    CloseConnection(conString);
                    Command.Dispose();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        #endregion

        #region[AgencyTDS]
        internal static DataTable AgencyList()
        {
            try
            {
                Command = new SqlCommand("SELECT Counter,Agency_Name,User_Id,Agent_Type,TDS,ExmptTDS,ExmptTdsLimit FROM agent_register", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyList");
                ObjDataTable = ObjDataSet.Tables["AgencyList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return ObjDataTable;
        }
        internal static bool UpdateAgencyTDS(int id, string tds)
        {
            try
            {
                Command = new SqlCommand("UPDATE agent_register SET TDS=" + tds + " , ExmptTDS=0,ExmptTdsLimit=0 WHERE counter=" + id, conString);
                Command.CommandType = CommandType.Text;
                OpenConnection(conString);
                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                if (result > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return false;
        }
        internal static DataTable GetAgencyType(string userType = "", string desc = "", string cmdType = "")
        {
            try
            {
                string msg = string.Empty;
                OpenConnection(conString);
                Command = new SqlCommand("usp_agentTypeMGMT", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UserType", !string.IsNullOrEmpty(userType) ? userType : "");
                Command.Parameters.AddWithValue("desc", !string.IsNullOrEmpty(desc) ? desc : "");
                Command.Parameters.AddWithValue("cmdType", !string.IsNullOrEmpty(cmdType) ? cmdType : "MultipleSelect");
                Command.Parameters.Add("@msg", SqlDbType.VarChar, 100);
                Command.Parameters["@msg"].Direction = ParameterDirection.Output;

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgentTypeMGMT");
                ObjDataTable = ObjDataSet.Tables["AgentTypeMGMT"];

                msg = Command.Parameters["@msg"].Value.ToString();

                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetSalesRef()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetSalesRef", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "SalesRef");
                ObjDataTable = ObjDataSet.Tables["SalesRef"];

                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetAgencyDetailsDynamic(Agency model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("AgencyDetailsDynamic", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UserId", !string.IsNullOrEmpty(model.FilterAgencyId) ? model.FilterAgencyId : "");
                Command.Parameters.AddWithValue("AgentType", !string.IsNullOrEmpty(model.FilterAgencyType) ? model.FilterAgencyType : "");
                Command.Parameters.AddWithValue("SalesExecID", !string.IsNullOrEmpty(model.FilterSalesRef) ? model.FilterSalesRef : "");
                Command.Parameters.AddWithValue("FromDate", !string.IsNullOrEmpty(model.FilterFromDate) ? model.FilterFromDate : "");//UtilityClass.FilterTodayDate()
                Command.Parameters.AddWithValue("ToDate", !string.IsNullOrEmpty(model.FilterToDate) ? model.FilterToDate : "");
                Command.Parameters.AddWithValue("DistrId", !string.IsNullOrEmpty(model.FilterDistrId) ? model.FilterDistrId : "");
                Command.Parameters.AddWithValue("UserType", !string.IsNullOrEmpty(model.FilterUserType) ? model.FilterUserType : "");
                Command.Parameters.AddWithValue("DiSearch", !string.IsNullOrEmpty(model.FilterDiSearch) ? model.FilterDiSearch : "");
                Command.Parameters.AddWithValue("DueAmount", !string.IsNullOrEmpty(model.FilterDueAmount) ? model.FilterDueAmount : "");
                Command.Parameters.AddWithValue("Email", !string.IsNullOrEmpty(model.FilterEmailId) ? model.FilterEmailId : "");
                Command.Parameters.AddWithValue("MobileNo", !string.IsNullOrEmpty(model.FilterMobile) ? model.FilterMobile : "");

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyDetails");
                ObjDataTable = ObjDataSet.Tables["AgencyDetails"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetAgencyDetailsByUserId(string userId)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("AgencyDetails_PP", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UserId", userId);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyDetails");
                ObjDataTable = ObjDataSet.Tables["AgencyDetails"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetDistrdetails()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetDistrdetails", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "Distrdetails");
                ObjDataTable = ObjDataSet.Tables["Distrdetails"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetAgentGroupType()
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("GetAgentType", conString);
                Command.CommandType = CommandType.StoredProcedure;

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgentType");
                ObjDataTable = ObjDataSet.Tables["AgentType"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                if (conString.State != ConnectionState.Closed)
                {
                    CloseConnection(conString);
                }
            }

            return ObjDataTable;
        }
        internal static DataTable GetStateList()
        {
            try
            {
                Command = new SqlCommand("select STATEID as Code,STATE as Name from Tbl_STATE where COUNTRY='India' order by STATE", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "StateList");
                ObjDataTable = ObjDataSet.Tables["StateList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return ObjDataTable;
        }
        internal static DataTable GetCityList(string stateCode)
        {
            try
            {
                Command = new SqlCommand("select CITY, STATEID from TBL_CITY  where STATEID='" + stateCode + "' order by CITY", conString);
                Command.CommandType = CommandType.Text;
                Adapter = new SqlDataAdapter();
                OpenConnection(conString);
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();
                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CityList");
                ObjDataTable = ObjDataSet.Tables["CityList"];
                CloseConnection(conString);
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
            return ObjDataTable;
        }
        internal static int UpdateAgencyDetail(SingleAgency_SP model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("UpdateAgentTypeSalesRef", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UID", model.User_Id);
                Command.Parameters.AddWithValue("SalesRef", !string.IsNullOrEmpty(model.SalesExecID) ? model.SalesExecID : "");
                Command.Parameters.AddWithValue("Type", model.Agent_Type);
                Command.Parameters.AddWithValue("AgentStatus", model.Agent_status);
                Command.Parameters.AddWithValue("Online_Tkt", model.Online_Tkt);
                Command.Parameters.AddWithValue("Address", model.Address);
                Command.Parameters.AddWithValue("City", model.City);
                Command.Parameters.AddWithValue("State", model.State);
                Command.Parameters.AddWithValue("Country", model.Country);
                Command.Parameters.AddWithValue("Zipcode", model.zipcode);
                Command.Parameters.AddWithValue("FName", model.Fname);
                Command.Parameters.AddWithValue("LName", model.Lname);
                Command.Parameters.AddWithValue("Mobile", model.Mobile);
                Command.Parameters.AddWithValue("Email", model.Email);
                Command.Parameters.AddWithValue("Fax", !string.IsNullOrEmpty(model.Fax_no)? model.Fax_no:"NA");
                Command.Parameters.AddWithValue("Pan", model.PanNo);
                Command.Parameters.AddWithValue("Title", model.Title);
                Command.Parameters.AddWithValue("AgencyName", model.Agency_Name);
                Command.Parameters.AddWithValue("pwd", model.PWD);
                Command.Parameters.AddWithValue("NamePanCard", model.NamePanCard);
                Command.Parameters.AddWithValue("OTPLoginStatus", (model.OTPLoginStatus == true ? "True" : "False"));
                Command.Parameters.AddWithValue("ExpPass", (model.PasswordExpMsg == true ? "True" : "False"));
                Command.Parameters.AddWithValue("IrctcId", model.IrctcId);

                Command.Parameters.AddWithValue("gstno", model.GSTNO);
                Command.Parameters.AddWithValue("gstcompanyname", model.GST_Company_Name);
                Command.Parameters.AddWithValue("gstcompanyAdd", model.GST_Company_Address);
                Command.Parameters.AddWithValue("gstPhone", model.GST_PhoneNo);
                Command.Parameters.AddWithValue("gstemail", model.GST_Email);
                Command.Parameters.AddWithValue("gstpin", model.GST_Pincode);
                Command.Parameters.AddWithValue("GstState", model.GST_State);
                Command.Parameters.AddWithValue("GstStateCode", model.GST_State_Code);
                Command.Parameters.AddWithValue("GstCity", model.GST_City);
                Command.Parameters.AddWithValue("gstApply", model.Is_GST_Apply);
                Command.Parameters.AddWithValue("distri", model.Distr);

                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                return result;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return 0;
        }
        #endregion
        internal static int InsertAgencyRegistration(AgencyDetail model)
        {
            try
            {
                OpenConnection(conString);
                Command = new SqlCommand("InsertRegistrationDetails_AgentReg_New", conString);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("user_id", !string.IsNullOrEmpty(model.User_Id) ? model.User_Id : "");
                Command.Parameters.AddWithValue("Title", !string.IsNullOrEmpty(model.Title) ? model.Title : "");
                Command.Parameters.AddWithValue("Fname", !string.IsNullOrEmpty(model.Fname) ? model.Fname : "");
                Command.Parameters.AddWithValue("Lname", !string.IsNullOrEmpty(model.Lname) ? model.Lname : "");
                Command.Parameters.AddWithValue("Address", !string.IsNullOrEmpty(model.Address) ? model.Address : "");
                Command.Parameters.AddWithValue("city", !string.IsNullOrEmpty(model.City) ? model.City : "");
                Command.Parameters.AddWithValue("state", !string.IsNullOrEmpty(model.State) ? model.State : "");
                Command.Parameters.AddWithValue("country", !string.IsNullOrEmpty(model.Country) ? model.Country : "");
                Command.Parameters.AddWithValue("Area", !string.IsNullOrEmpty(model.Area) ? model.Area : "");
                Command.Parameters.AddWithValue("zipcode", !string.IsNullOrEmpty(model.zipcode) ? model.zipcode : "");
                Command.Parameters.AddWithValue("Phone", !string.IsNullOrEmpty(model.Phone) ? model.Phone : "");
                Command.Parameters.AddWithValue("Mobile", !string.IsNullOrEmpty(model.Mobile) ? model.Mobile : "");
                Command.Parameters.AddWithValue("email", !string.IsNullOrEmpty(model.Email) ? model.Email : "");
                Command.Parameters.AddWithValue("WhMobile", !string.IsNullOrEmpty(model.WhMobile) ? model.WhMobile : "");

                Command.Parameters.AddWithValue("Alt_Email", !string.IsNullOrEmpty(model.Alt_Email) ? model.Alt_Email : "NA");
                Command.Parameters.AddWithValue("Fax_no", !string.IsNullOrEmpty(model.Fax_no) ? model.Fax_no : "NA");
                Command.Parameters.AddWithValue("Agency_Name", !string.IsNullOrEmpty(model.Agency_Name) ? model.Agency_Name : "");
                Command.Parameters.AddWithValue("Website", !string.IsNullOrEmpty(model.Website) ? model.Website : "NA");
                Command.Parameters.AddWithValue("NameOnPan", !string.IsNullOrEmpty(model.NamePanCard) ? model.NamePanCard : "");
                Command.Parameters.AddWithValue("PanNo", !string.IsNullOrEmpty(model.PanNo) ? model.PanNo.ToUpper() : "");
                Command.Parameters.AddWithValue("Status", !string.IsNullOrEmpty(model.Status) ? model.Status : "TA");
                Command.Parameters.AddWithValue("Stax_no", !string.IsNullOrEmpty(model.Stax_no) ? model.Stax_no : "NA");
                Command.Parameters.AddWithValue("Remark", !string.IsNullOrEmpty(model.Remark) ? model.Remark : "NA");

                Command.Parameters.AddWithValue("Sec_Qes", !string.IsNullOrEmpty(model.Sec_Qes) ? model.Sec_Qes : "NA");
                Command.Parameters.AddWithValue("PWD", !string.IsNullOrEmpty(model.PWD) ? model.PWD : "");
                Command.Parameters.AddWithValue("Agent_Type", !string.IsNullOrEmpty(model.Agent_Type) ? model.Agent_Type : ""); //think
                Command.Parameters.AddWithValue("Distr", !string.IsNullOrEmpty(model.Distr) ? model.Distr : "HEADOFFICE");
                Command.Parameters.AddWithValue("SalesExecID", !string.IsNullOrEmpty(model.SalesExecID) ? model.SalesExecID : "");
                Command.Parameters.AddWithValue("ag_logo", !string.IsNullOrEmpty(model.ag_logo) ? model.ag_logo : "");
                Command.Parameters.AddWithValue("Branch", !string.IsNullOrEmpty(model.Branch) ? model.Branch : "");

                Command.Parameters.AddWithValue("GSTNO", !string.IsNullOrEmpty(model.GSTNO) ? model.GSTNO : "");
                Command.Parameters.AddWithValue("GSTCompanyName", !string.IsNullOrEmpty(model.GST_Company_Name) ? model.GST_Company_Name : "");
                Command.Parameters.AddWithValue("GSTCompanyAddress", !string.IsNullOrEmpty(model.GST_Company_Address) ? model.GST_Company_Address : "");
                Command.Parameters.AddWithValue("GSTPhoneNo", !string.IsNullOrEmpty(model.GST_PhoneNo) ? model.GST_PhoneNo : "");
                Command.Parameters.AddWithValue("GSTEmail", !string.IsNullOrEmpty(model.GST_Email) ? model.GST_Email : "");
                Command.Parameters.AddWithValue("IsGSTApply", !string.IsNullOrEmpty(model.Is_GST_Apply) ? model.Is_GST_Apply : "0");
                Command.Parameters.AddWithValue("GSTCity", !string.IsNullOrEmpty(model.GST_City) ? model.GST_City : "");
                Command.Parameters.AddWithValue("GSTState", !string.IsNullOrEmpty(model.GST_State) ? model.GST_State : "");
                Command.Parameters.AddWithValue("GSTStateCode", !string.IsNullOrEmpty(model.GST_State_Code) ? model.GST_State_Code : "");
                Command.Parameters.AddWithValue("GSTPinCode", !string.IsNullOrEmpty(model.GST_Pincode) ? model.GST_Pincode : "");

                int result = Command.ExecuteNonQuery();
                CloseConnection(conString);
                return result;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return 0;
        }
    }
}